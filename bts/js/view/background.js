koko.class('BackgroundView', 'view', {
    BackgroundView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var t = this, g = app.guiSize, vp = app.viewPort;
        
        this.children = koko.display.buildLayout([
			{ name:'bg1', id:'Background', type:'sprite' },
			{ name:'bg2', id:'Background', type:'sprite', x:-2 }
        ], this.container);
        
        this.bg_elements = [ this.children.bg1, this.children.bg2];
    },
    move: function(position, speed) {
		var width = 1134;
        for( var i = 0; i < this.bg_elements.length; i++ ){
            var cont = this.bg_elements[i];
            //boom
            cont.position.x = (position * speed) + (width*i); //568 itm width
            //modulo
            cont.position.x %= width * this.bg_elements.length; 
            if(cont.position.x < 0) cont.position.x += width * this.bg_elements.length;
            //tak awee witf
            cont.position.x = Math.round( cont.position.x - width );
        }
    },
    transitionIn: function(){
        this.buildView();
        this.show();
        
        app.addUpdateListener('background', function(){
            this.move(game.xpos*-1, 1);
        }.bind(this));
    },
    transitionOut: function(){
        this.destroyView();
        this.hide();
        
        app.removeUpdateListener('background');
    },
	clearBackgroundItems : function(){
			
	}
});