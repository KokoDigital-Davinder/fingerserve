koko.class('PausedGameView', 'view', {
    PausedGameView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
        this.children = koko.display.buildLayout([
			{ name:'bg', type:'shape', alpha:0.7, fill:0x0, width:g.width, height:g.height },
			
			{ name:'main_menu_btn', type:'sprite', id:'MainMenuLargeButton', y:vp.centerY, x:vp.centerX - 186 - 30, regX:93, regY:93 },
			{ name:'restart_btn', type:'sprite', id:'RestartButton', y:vp.centerY, x:vp.centerX, regX:93, regY:93 },
			{ name:'resume_btn', type:'sprite', id:'ResumeButton', y:vp.centerY, x:vp.centerX + 186 + 30, regX:93, regY:93 },
		], this.container);
		
		var main_menu_btn = new Button(this.children.main_menu_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut();
                v.get('background').transitionOut();
                v.get('background').transitionIn();
                game.backToMenu();
				v.get('ingame').transitionOut();
				v.get('mainmenu').transitionIn();
                
			}.bind(this)
		});

		var restart_btn = new Button(this.children.restart_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut()
                v.get('background').transitionOut();
                v.get('background').transitionIn();
                game.backToMenu();
				v.get('ingame').transitionOut();
				v.get('ingame').transitionIn();
                
			}.bind(this)
		});
				
		var resume_btn = new Button(this.children.resume_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut();
				game.paused = false;
			}.bind(this)
		});
		
        this.elements = [ main_menu_btn, restart_btn, resume_btn ];
    },
    transitionIn: function(){
		this.buildView();
		this.show();
		this.enable();
		v.each(['menuhud', 'ingame'], 'disable');
    },
    transitionOut: function(){
		this.destroyView();  
		this.hide();
		v.each(['menuhud', 'ingame'], 'enable');
    }
});