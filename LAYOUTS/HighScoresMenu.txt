[
{ name:'HighScoresContainer', type:'container', id:'HighScoresContainer', x:0, y:0 },
{ name:'Background', type:'sprite', id:'Background', parent:'HighScoresContainer', x:0, y:0 },
{ name:'HighscoresPanel', type:'container', id:'HighscoresPanel', parent:'HighScoresContainer', x:567, y:380 },
{ name:'HighscoresBack', type:'sprite', id:'HighscoresBack', parent:'HighscoresPanel', x:-1, y:2, regX:390, regY:296 },
{ name:'HighscoreEntry', type:'container', id:'HighscoreEntry', parent:'HighscoresPanel', x:0, y:-155 },
{ name:'HighscoresHighlights', type:'container', id:'HighscoresHighlights', parent:'HighscoreEntry', x:0, y:0 },
{ name:'HighlightBlue', type:'sprite', id:'HighlightBlue', parent:'HighscoresHighlights', x:0, y:0, regX:353, regY:45 },
{ name:'HighlightWhite', type:'sprite', id:'HighlightWhite', parent:'HighscoresHighlights', x:0, y:0, regX:353, regY:45 },
{ name:'HighscoresRankTextfield', type:'bmptext', parent:'HighscoreEntry', x:-294, y:0, text:'1
', size:'45px', font:'Londrina Solid', lw:52, tint:0xa6e1, regX:26, regY:28 },
{ name:'HighscoresDistanceTextfield', type:'bmptext', parent:'HighscoreEntry', x:157, y:0, text:'256m
', size:'45px', font:'Londrina Solid', lw:163, tint:0xa6e1, regX:108, regY:28 },
{ name:'HighscoresNameTextfield', type:'bmptext', parent:'HighscoreEntry', x:-38, y:0, text:'Name
', size:'45px', font:'Londrina Solid', lw:223, tint:0x0, regX:112, regY:28 },
{ name:'HighscoresTotalStars', type:'container', id:'HighscoresTotalStars', parent:'HighscoreEntry', x:274, y:2, s:0.89 },
{ name:'StarsTotalBack', type:'sprite', id:'StarsTotalBack', parent:'HighscoresTotalStars', x:-1, y:-1, regX:40, regY:41 },
{ name:'StarsTotalTextfield', type:'bmptext', parent:'HighscoresTotalStars', x:-4, y:-3, text:'22', size:'32px', font:'Londrina Solid', lw:73, tint:0xffffff, regX:37, regY:20 },
{ name:'HighscoresTitleTextfield', type:'bmptext', parent:'HighscoresPanel', x:-2, y:-256, text:'HIGHSCORES', size:'45px', font:'Londrina Solid', lw:407, tint:0xf24321, regX:204, regY:28 },
{ name:'PreviousButton', type:'sprite', id:'PreviousButton', parent:'HighScoresContainer', x:128, y:698, regX:45, regY:45 },
{ name:'CloseButton', type:'sprite', id:'CloseButton', parent:'HighScoresContainer', x:128, y:72, regX:45, regY:45 },
{ name:'StarsTotal', type:'container', id:'StarsTotal', parent:'HighScoresContainer', x:1008, y:74 },
{ name:'StarsTotalBack', type:'sprite', id:'StarsTotalBack', parent:'StarsTotal', x:-1, y:-1, regX:40, regY:41 },
{ name:'StarsTotalTextfield', type:'bmptext', parent:'StarsTotal', x:-4, y:-3, text:'22', size:'32px', font:'Londrina Solid', lw:73, tint:0xffffff, regX:37, regY:20 }
]