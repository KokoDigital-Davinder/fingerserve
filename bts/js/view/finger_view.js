koko.class('FingerView', 'view', {
    assets: [],
    FingerView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var t = this, g = app.guiSize, vp = app.viewPort;
        
		var colour = ['blue', 'green', 'orange', 'red'][user.char ? user.char : 0];
		
		this.container.x = -280;
		this.container.y = 50;
		
        this.children = koko.display.buildLayout([
			
			{ name:'hand_cont' },
			{ name:'hand', parent:'hand_cont', type:'sprite', id:'hand_1', regX:30, regY:30 },
			
			{ name:'hand1', parent:'hand_cont', type:'sprite', id:'hand_2', regX:-54.95, regY:-10 },
			
			{ name:'finger_1', type:'sprite', id:'finger_1_'+colour+'_1', parent:'hand_cont' },
			{ name:'finger_2', type:'sprite', id:'finger_1_'+colour+'_5', parent:'hand_cont' },
			{ name:'finger_3', type:'sprite', id:'finger_1_'+colour+'_7', parent:'hand_cont' },
			{ name:'finger_4', type:'sprite', id:'finger_1_'+colour+'_8', parent:'hand_cont' },
			
			
			{ name:'finger_5',  type:'sprite', id:'finger_2_'+colour+'_1', parent:'hand_cont' },
			{ name:'finger_6', type:'sprite', id:'finger_2_'+colour+'_2', parent:'hand_cont' },
			{ name:'finger_7', type:'sprite', id:'finger_2_'+colour+'_3', parent:'hand_cont' },
			{ name:'finger_8', type:'sprite', id:'finger_2_'+colour+'_4', parent:'hand_cont' },
			{ name:'finger_9', type:'sprite', id:'finger_2_'+colour+'_5', parent:'hand_cont' },
			{ name:'finger_10',  type:'sprite', id:'finger_2_'+colour+'_9', parent:'hand_cont' },
			{ name:'finger_11', type:'sprite', id:'finger_2_'+colour+'_10', parent:'hand_cont' },
			
			
			{ name:'racket', parent:'hand_cont' },
				{ name:'handle', parent:'racket', type:'sprite', id:'racket_1_handle_'+colour, x:-1, y:16},
				{ name:'bat', parent:'racket', type:'sprite', id:'racket_1_end', x:101-1, y:-18+16 },
			
			
			{ name:'racket1', parent:'hand_cont' },
				{ name:'handle', parent:'racket1', type:'sprite', id:'racket_2_handle_'+colour, x:1, y:-33.85},
				{ name:'bat', parent:'racket1', type:'sprite', id:'racket_2_end', x:86, y:-58 },
			
			{ name:'racket2', parent:'hand_cont' },
				{ name:'handle', parent:'racket2', type:'sprite', id:'racket_3_handle_'+colour, x:0, y:29},
				{ name:'bat', parent:'racket2', type:'sprite', id:'racket_3_end', x:91, y:0 },
        ], this.container);
		

		this.currentFrame = 0;
		
		this.animations = {
			keepup: [
				{
					hand: { visible:true, x:-4.95, y:-6.50, rotation:0 },
					hand1: { visible:false },
					finger_1: { visible:true, x:78, y:106, rotation:0 },
					finger_2: { visible:false },
					finger_3: { visible:false },
					finger_4: { visible:false },
					finger_5: { visible:false },
					finger_6: { visible:false },
					finger_7: { visible:false },
					finger_8: { visible:false },
					finger_9: { visible:false },
					finger_10: { visible:false },
					finger_11: { visible:false },
					racket: { visible:true, rotation:30, x:177, y:161.95 },
					racket1: { visible:false },
					racket2: { visible:false },
				},
				{
					hand: { rotation:-1.2 },
					finger_1: { visible:false },
					finger_2: { visible:true, x:82.2, y:101 },
					racket: { rotation:21, x:182, y:155 },
				},
				{
					hand: { rotation:-2.5 },
					finger_2: { visible:false },
					finger_3: { visible:true, x:84.3, y:97  },
					racket: { rotation:12.1, x:186.65, y:148 },
				},
				{
					hand: { rotation:-5.2 },
					finger_3: { visible:false },
					finger_4: { visible:true, x:89.95, y:89 },
					racket: { rotation:-0.9, x:194.45, y:136 },
				},
				{},
				{},
				{},
				{},
				{
					hand: { rotation:-2.5 },
					finger_3: { visible:true },
					finger_4: { visible:false },
					racket: { rotation:12.1, x:186.65, y:148 },
				},
				{
					hand: { rotation:-1.2 },
					finger_2: { visible:true },
					finger_3: { visible:false },
					racket: { rotation:21, x:182, y:155 },
				}
			],
			hit: [
				{
					hand: { visible:false },
					hand1: { visible:true, x:0, y:-7.7 },
					finger_1: { visible:false  },
					finger_2: { visible:false },
					finger_3: { visible:false },
					finger_4: { visible:false },
					finger_5: { visible:true, x:94.45, y:173.1 },
					finger_6: { visible:false },
					finger_7: { visible:false },
					finger_8: { visible:false },
					finger_9: { visible:false },
					finger_10: { visible:false },
					finger_11: { visible:false },
					racket: { visible:false },
					racket1: { visible:true, rotation:75, x:148, y:261.2 },
					racket2: { visible:false },
				},
				{
					hand1: { rotation:-15 },
					racket1: { visible:true, rotation:86, x:188.6, y:213.9-10 },
					finger_5: { visible:false },
					finger_6: { visible:true, x:136.6, y:133.55-10},
				},
				{
					hand: { visible:true, rotation:3.8, x:50, y:3.5 },
					hand1: { visible:false },
					racket: { visible:true, rotation:61.2, x:204, y:201.6 },
					racket1: { visible:false },
					finger_6: { visible:false },
					finger_7: { visible:true, x:106.4, y:132.5 },
				},
				{
					hand: { rotation:0, x:50, y:3.5 },
					racket: { rotation:-4, x:234.45, y:168.55 },
					finger_7: { visible:false },
					finger_8: { visible:true, x:133.35, y:111.15 },
				},
				{
					hand: { rotation:-3.9, x:50, y:3.5 },
					racket: { visible:false },
					racket2: { visible:true, rotation:-35.7, x:222.95, y:117.6 },
					finger_8: { visible:false },
					finger_9: { visible:true, x:144.35, y:98.95 },
				},
				{
				},
				{
					hand: { visible:true, rotation:3.8, x:50, y:3.5 },
					hand1: { visible:false },
					racket: { visible:true, rotation:42.0, x:152.85+52, y:200 },
					racket2: { visible:false },
					finger_9: {visible:false},
					finger_10: {visible:true, x:120.05, y:131.5},
				},
				{
					hand: { visible:false },
					hand1: { visible:true, x:0, y:2.3, rotation:-6.8 },
					racket: { visible:true, rotation:61.2, x:204-5, y:201.6+26 },
					racket2: { visible:false },
					finger_10: { visible:false },
					finger_11: { visible:true, x:61.9+54, y:156.7 },
				},
				{
					racket: { visible: false },
					hand1: { x:0, y:-7.7, rotation:0 },
					racket1: { visible:true, rotation:75, x:148, y:261.2 },
					finger_11: {visible:false },
					finger_5: {visible:true, x:94.45, y:173.1},
				},
				{}
			]
		};
		
		this.setIdle();
		this.frames = null;
		this.listenerName = 'finger'+Math.random();
		app.addUpdateListener(this.listenerName, this.update.bind(this));
    },
	isPlaying: false,
	playAnim: function(anim){
		this.isPlaying = true;
		this.currentFrame = 0;
		this.frames = this.animations[anim];
		
		this.currentAnim = anim;
		
		this.speed = 1.4;
		if(anim=='hit') this.speed = 0.9;
		
	},
	setIdle: function(){
		this.isPlaying = false;
		for( var key in this.animations.keepup[0] ) {
			d.applyProps(this.children[key], this.animations.keepup[0][key]);
		}	
	},
	update: function(delta){
		if(!this.isPlaying) return;
		
		var f = Math.floor(this.currentFrame);
		for( var key in this.frames[f] ) {
			d.applyProps(this.children[key], this.frames[f][key]);
		}
				
		this.currentFrame += delta ? (delta*this.speed) : 0;
		if(this.currentFrame > this.frames.length) {
			this.currentFrame = 0;
			this.update(0);
			this.isPlaying = false;
		}
	},
    transitionIn: function(){
        this.buildView();
        this.show();
        this.enable();
    },
	transitionOut: function(){
		this.disable();
		this.hide();
		this.destroy();
		app.removeUpdateListener(this.listenerName);
	}
});