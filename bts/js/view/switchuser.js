koko.class('SwitchUserView', 'view', {
    SwitchUserView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
          var texture = PIXI.Texture.fromImage(koko.assets.getAssetURL('av'+user.id), true);
            var avatarScale = 138 / texture.width;
        
        
        this.children = koko.display.buildLayout([
			{ name:'bg', type:'shape', alpha:0.7, fill:0x0, width:g.width, height:g.height },
			
			{ name:'panel_cont', type:'container', x:vp.centerX, y:vp.centerY, regX:312, regY:220 },
			
            { name:'panel_back', type:'sprite', id:'SwitchUserPanelBack', parent:'panel_cont' },
            { name:'title_txt', type:'bmptext', parent:'panel_cont', fitWidth: 486, text:lang.getTerm('likeswitchuser'), size:'35px', font:lang.getFontName(), tint:0x0, x:312, y: 220, align:'center' },

				{ name:'no_btn', type:'container', parent:'panel_cont', x:187, y: 337 },
					{ name:'no_btn_bg', type:'sprite', id:'YesNoButtonBack', parent:'no_btn', regX:122, regY:57 },
					{ name:'no_btn_txt', type:'bmptext', parent:'no_btn',text:lang.getTerm('no'), size:'50px', font:lang.getFontName(), tint:0xffffff, align:'center', y:-25 },

				{ name:'yes_btn', type:'container', parent:'panel_cont', x:437, y: 337  },
					{ name:'yes_btn_bg', type:'sprite', id:'YesNoButtonBack', parent:'yes_btn', regX:122, regY:57 },
					{ name:'yes_btn_txt', type:'bmptext', parent:'yes_btn', text:lang.getTerm('yes'), size:'50px', font:lang.getFontName(), tint:0xffffff, align:'center', y:-25 },
                    { name:'NowPlayingAsImage', type:'bitmap', parent:'panel_back', id:'av'+user.id, x:125, y:50, scale:avatarScale}
			
		], this.container);
		
		var no_btn = new Button(this.children.no_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound(); 
				this.transitionOut();
			}.bind(this)
		});
		
		var yes_btn = new Button(this.children.yes_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound(); 
				this.transitionOut();
                v.get('selectuser').transitionIn();
				//user.selectUser(user.userNames[m.randomInt(0, user.userNames.length-1)]);
				//v.get('menuhud').setState('mainmenu');
				//v.get('userloggedin').transitionIn();
			}.bind(this)
		});
		
		
        this.elements = [ no_btn, yes_btn ];
    },
    transitionIn: function(){
		var vp = app.viewPort;
		
		this.buildView();
		this.show();
		
		v.all(function(v){
			if(v.container.visible) v.disable();
		});
		
		d.applyThenAnimate(this.container, { alpha:0 }, .3, { alpha:1 });
		d.applyThenAnimate(this.children.panel_cont, { y:vp.centerY+20, alpha:0 }, .3, { delay:0.1, alpha:1, y:vp.centerY });
		
		this.enable();
    },
    transitionOut: function(){
		this.disable();
		d.animateObject(this.container, .2, { alpha: 0, onComplete: function(){
			
			v.all(function(v){
				if(v.container.visible) v.enable();
			});

			this.destroyView();  
			this.hide();
			
		}.bind(this)});
    }
});