koko.class('collidable', '', {
	//positoin
    pos: null,
	img_offset: null,
    //reaction
    speedImpact: null,
    minSpeedImpact: null,
    //gfx
    movie:null,
    //def
	hasCollided: false,
	shouldResetHitState: false,
	soundRef: "",
    textureNames:null,
    soundName:'',
    frameDef: null,
	collidable: function(ref){
        this.pos = new PIXI.math.Rectangle();
        this.img_offset = new PIXI.math.Point();
		
		this.frameDef = {
            //animName: [loop, frameStart, frameEnd],
            idle: [false, 0, 0],
            hit: [false, 1, 1]	
        }
        
        this.speedImpact = new PIXI.math.Point();
        this.minSpeedImpact = new PIXI.math.Point();
	},
    createMovieclip: function(){
        var textures = [];
        for( var i = 0; i < this.textureNames.length; i++ ) {
            textures.push(new PIXI.Texture.fromFrame(this.textureNames[i]));
        }
        this.movie = new PIXI.extras.MovieClip(textures);
        this.clipManager = new clipManager(this.movie, this.frameDef);
		this.clipManager.gotoAndStop('idle');
    },
	drawDebug:function(){
		var s = new PIXI.Graphics();
		s.beginFill(0x000000, .5);
		s.drawRect(0, 0, this.pos.width, this.pos.height);
		this.movie.parent.addChild(s);
		s.x = this.movie.x + (this.img_offset.x * -1);
		s.y = this.movie.y + (this.img_offset.y * -1);
	},
    setPosition: function(x){
        this.pos.x = x;   
    },
    collide: function(){
        soundManager[this.soundRef]();
        this.hasCollided = true;
		this.clipManager.gotoAndPlay('hit');
    },
    destroy: function(){
        if(this.movie.parent) this.movie.parent.removeChild(this.movie);
        this.clipManager.destroy();
    },
    update: function(){
        this.clipManager.update();  
    }
}); ///COLLIDABLE DEFINITION
koko.class('KinderinoBoost', 'collidable', {
   KinderinoBoost: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playWhooshSound';
        //create imgs 
        this.textureNames = ['bst_kinderino_swing1.png', 'bst_kinderino_swing2.png'];
        
        this.createMovieclip();
	   
	    //reaction
        this.speedImpact.x = 1.5;
        this.speedImpact.y = 1.5;
        //min speed after impact
        this.minSpeedImpact.x = 15;
        this.minSpeedImpact.y = -15;
        
        //box
        this.pos.width = 310, this.pos.height = 369, this.pos.y = GAME_CONST.floorY-369;
    }
}); //done
koko.class('ManholeBoost', 'collidable', {
   ManholeBoost: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playBoing3';
        //create imgs 
        this.textureNames = ['bst_manhole1.png', 'bst_manhole2.png'];
        this.createMovieclip();
	   
	    //reaction
        this.speedImpact.x = 1.4;
        this.speedImpact.y = 1.4;
	   
        //min speed after impact
        this.minSpeedImpact.x = 10;
        this.minSpeedImpact.y = -20;
	   
	    //img offset
	    this.img_offset.x = 0;
	    this.img_offset.y = -62;
        
        //box
        this.pos.width = 195, this.pos.height = 71, this.pos.y = GAME_CONST.floorY-50;
    }
}); //done
koko.class('MoleBoost', 'collidable', {
   MoleBoost: function() {
        //super
        this.superr.collidable.call(this);
       this.soundRef = 'playBoing4';
        //create imgs 
        this.textureNames = ['bst_mole1.png', 'bst_mole2.png'];
        this.createMovieclip();
	   
	   //reaction
        this.speedImpact.x = 1.2;
        this.speedImpact.y = 2.2;
        //min speed after impact
        this.minSpeedImpact.x = 4;
        this.minSpeedImpact.y = -15;
	   //img offset
	   this.img_offset.x = 0;
	   this.img_offset.y = -43;
        
        //box
        this.pos.width = 228, this.pos.height = 70, this.pos.y = GAME_CONST.floorY-50;
    }
}); //done
koko.class('ParasolBoost', 'collidable', {
   ParasolBoost: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playBoing5';
        //create imgs 
        this.textureNames = ['bst_parasol1.png', 'bst_parasol2.png'];
        this.createMovieclip();
	   
        //reaction
        this.speedImpact.x = 1.1;
        this.speedImpact.y = 1.5;
	   
        //min speed after impact
        this.minSpeedImpact.x = 5;
        this.minSpeedImpact.y = -22;
	   
	    this.img_offset.x = -150; //356-56*-.5

	    //box
        this.pos.width = 56, this.pos.height = 314, this.pos.y = GAME_CONST.floorY-290;
    }
}); //done
koko.class('NetBoost', 'collidable', {
   NetBoost: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playBoing6';
        //create imgs 
        this.textureNames = ['bst_springnet1.png', 'bst_springnet2.png'];
        this.createMovieclip();
	   
	    //reaction
        this.speedImpact.x = 1.2;
        this.speedImpact.y = 1.8;
        
	    //min speed after impact
        this.minSpeedImpact.x = 5;
        this.minSpeedImpact.y = -15;
	   
	   this.img_offset.x = -8; //356-56*-.5
	   this.img_offset.y = -11; //356-56*-.5
        
        //box
        this.pos.width = 319, this.pos.height = 120, this.pos.y = GAME_CONST.floorY-95;
    }
}); //done
koko.class('GymBagObstacle', 'collidable', {
   GymBagObstacle: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playObstacleSound';
        //create imgs 
        this.textureNames = ['obs_gymbag.png'];
        this.frameDef.hit = [false, 0, 0];	
        this.createMovieclip();
	   
	    //reaction
        this.speedImpact.x = -0.1;
        this.speedImpact.y = 0.2;
        
	   //poo
	   this.img_offset.x = -10;
	   this.img_offset.y = -11;
        
        //box
        this.pos.width = 179, this.pos.height = 121, this.pos.y = GAME_CONST.floorY-100;
		;
    }
}); //done
koko.class('ChairObstacle', 'collidable', {
   ChairObstacle: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playObstacleSound';
        //create imgs 
        this.textureNames = ['obs_umpirechair.png'];
        this.frameDef.hit = [false, 0, 0];
        this.createMovieclip();
	   
	   //reaction
        this.speedImpact.x = -0.1;
        this.speedImpact.y = 1;
	   
	   //img
	   this.img_offset.x = -17;
	   this.img_offset.y = 0;
	   
        //box
        this.pos.width = 176, this.pos.height = 284, this.pos.y = GAME_CONST.floorY-250;
    }
}); //done
koko.class('BinObstacle', 'collidable', {
   BinObstacle: function() {
        //super
        this.superr.collidable.call(this);
        this.soundRef = 'playObstacleSound';
        //create imgs 
        this.textureNames = ['obs_wastebin.png'];
        this.frameDef.hit = [false, 0, 0];
        this.createMovieclip();
	   
	   //reaction
        this.speedImpact.x = -0.1;
        this.speedImpact.y = 1;
	   
	   this.img_offset.x = 15;
	   
        //box
        this.pos.width = 149, this.pos.height = 175, this.pos.y = GAME_CONST.floorY-150;
    }
}); //done