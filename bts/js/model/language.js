koko.class('languageController', '', {
	code: null,
	fonts: {
		en: {
			src: 'img/fonts/londrina/londrinafont.fnt', //DONE
			name:'Londrina Solid',
            alwaysUse:true,
			upper:true
		},
		it: {
			src: 'img/fonts/londrina/londrinafont.fnt',//DONE
			name:'Londrina Solid',
            alwaysUse:true,
			upper:true
		},
		fr: {
			src: 'img/fonts/londrina/londrinafont.fnt',//DONE
			name:'Londrina Solid',
            alwaysUse:true,
			upper:true
		},
		es: {
			src: 'img/fonts/londrina/londrinafont.fnt',//DONE
			name:'Londrina Solid',
            alwaysUse:true,
			upper:true
		},
		pt: {
			src: 'img/fonts/londrina/londrinafont.fnt',//DONE
			name:'Londrina Solid',
            alwaysUse:true,
			upper:true
		},
		de: {
			src: 'img/fonts/londrina/londrinafont.fnt',//DONE
			name:'Londrina Solid',
            alwaysUse:true,
			upper:true
		},
		pl: {
			src: 'img/fonts/loyola/loyolafont.fnt',//DONE
			name:'Loyola'
		},
		ru: {
			src: 'img/fonts/loyola/loyolafont.fnt',//DONE
			name:'Loyola'
		},
		ar: {
			src: 'img/fonts/arabic/arabicfont.fnt',
			name:'Arial',
			ltr: true
		},
		cn: {
			src: 'img/fonts/chinese/chinesefont.fnt',
			name:'SimSun'
		},
		he: {
			src: 'img/fonts/myriadhebrew/myriadhebrewfont.fnt',
			name:'Myriad Hebrew',
			ltr: true
		}
	},
	terms: {
		logo_sprite_id: {
            skipReverse:true,
			en: 'GameLogo_en',
			it:'GameLogo_it',
			fr:'GameLogo_fr',
			es:'GameLogo_es',
			pt:'GameLogo_prt',
			de:'GameLogo_de',
			pl:'GameLogo_pl',
			ru:'GameLogo_ru',
			cn:'GameLogo_cn',
			ar:'GameLogo_ar',
			he:'GameLogo_he',
		},
		new_best_sprite_id: {
            skipReverse:true,
			en: 'go_newbest_en',
			it:'go_newbest_it',
			fr:'go_newbest_fr',
			es:'go_newbest_es',
			pt:'go_newbest_pt',
			de:'go_newbest_de',
			pl:'go_newbest_pl',
			ru:'go_newbest_ru',
			cn:'go_newbest_cn',
			ar:'go_newbest_ar',
			he:'go_newbest_iw',
		},
		welldone: {
			upper:true,
			en: 'Well done!',
			it:'Ben fatto!',
			fr:'Bien joué !',
			es:'¡Felicidades!',
			pt:'Muito bem!',
			de:'Gut gemacht!',
			pl:'Dobra robota!',
			ru:'Отличная работа!',
			cn:'干得漂亮！',
			ar:'أحسنت!',
			he:'מצוין!',
		},
        guest: {
			en:'Guest',
			it:'Ospite',
			fr:'Invité',
			es:'Invitado',
			pt:'Convidado',
			de:'Gäste',
			pl:'Gość',
			ru:'Гость',
			cn:'游客',
			ar:'ضيف',
			he:'אורח',
		},
		selectdog: {
			upper:true,
			en:'Choose a colour',
			it:'Scegli un colore',	
			fr:'Choisis une couleur',	
			es:'Elige un color',
			pt:'Escolhe uma cor',
			de:'Wähle eine Farbe',
			pl:'Wybierz kolor',
			ru:'Выбери цвет',
			cn:'选择颜色',
			ar:'اختر لونًا',
			he:'בחרו צבע',
		},
		highscores: {
			upper:true,
			en:'Highscores',
			it:'Tabella punteggi',
			fr:'Scores',
			es:'Marcador',
			pt:'Pontuações',
			de:'Höchstpunktzahlen',
			pl:'Najlepsze wyniki',
			ru:'Таблица рекордов',
			cn:'最高分',
			ar:'النتائج العالية',
			he:'טבלת המובילים',
		},
		yourbestthrow: {
			en:'Your best throw',
			it:'Il tuo lancio migliore',
			fr:'Ton meilleur lancer',
			es:'Tu mejor tiro',
			pt:'O teu melhor lançamento',
			de:'Dein bester Wurf',
			pl:'Twój najlepszy rzut',
			ru:'Твой лучший бросок',
			cn:'最佳发球',
			ar:'أفضل رمية لك',
			he:'הזריקה הטובה ביותר שלכם',
		},
		yourpb: {
			en:'Your personal best',
			it:'Il tuo record',
			fr:'Ton record',
			es:'Tu récord',
			pt:'O teu melhor recorde',
			de:'Deine persönliche Bestleistung',
			pl:'Twój rekord życiowy',
			ru:'Твой личный рекорд',
			cn:'个人最佳成绩',
			ar:'أفضل نتيجة لك',
			he:'השיא האישי שלכם',
		},
		howtoplay: {
			upper:true,
			en:'How to play',
			it:'Modalità di gioco',
			fr:'Comment jouer ?',
			es:'Instrucciones',
			pt:'Instruções',
			de:'Spielanleitung',
			pl:'Jak grać',
			ru:'Как играть',
			cn:'游戏方法',
			ar:'كيفية اللعب',
			he:'הוראות',
		},
		distance: {
			upper:true,
			en:'Distance',
			it:'Distanza',
			fr:'Distance',
			es:'Distancia',
			pt:'Distância',
			de:'Distanz',
			pl:'Odległość',
			ru:'Дистанция',
			cn:'距离',
			ar:'المسافة',
			he:'מרחק',
		},
		windspeed: {
			upper:true,
			en:'Wind speed',
			it:'Velocità del vento',
			fr:'Vitesse du vent',
			es:'Velocidad del viento',
			pt:'Velocidade do vento',
			de:'Windgeschwindigkeit',
			pl:'Prędkość wiatru',
			ru:'Скорость ветра',
			cn:'风速',
			ar:'سرعة الرياح',
			he:'מהירות הרוח',
		},
		mph: {
			upper:true,
			en:'MPH',
			it:'KM/H',
			fr:'M/H',
			es:'KM/H',
			pt:'KM/H',
			de:'KM/H',
			pl:'KM/GODZ',
			ru:'МИ/Ч',
			cn:'英里/小时',
			ar:'ميل/ثانية',
			he:'מייל לשעה',
		},
		m: {
			en:'pM',
			it:'pM',
			fr:'pM',
			es:'pM',
			pt:'pM',
			de:'pM',
			pl:'pM',
			ru:'pM',
			cn:'p 米',
			ar:'p مت',
			he:'p מטר',
		},
		youreached: {
			en:'You reached',
			it:'Hai raggiunto',
			fr:'Tu as atteint',
			es:'Has alcanzado',
			pt:'Alcançaste',
			de:'Deine Punktzahl',
			pl:'Uzyskano',
			ru:'Получилось',
			cn:'你做到了',
			ar:'لقد وصلت',
			he:'הגעתם ל-',
		},
		foulthrow: {
			upper:true,
			en:'Foul throw',
			it:'Tiro fallo',
			fr:'Faux lancer',
			es:'¡Falta!',
			pt:'Lançamento falhado',
			de:'Schlechter Wurf',
			pl:'Zły wrzut z autu',
			ru:'Неудачный бросок',
			cn:'发球违规',
			ar:'رمية تماس',
			he:'זריקת עונשין',
		},
		attempt: {
			upper:true,
			en:'Attempt #',
			it:'Tentativo #',
			fr:'Tentative #',
			es:'Intento #',
			pt:'Tentativa #',
			de:'Versuch #',
			pl:'Próba #',
			ru:'Попытка #',
			cn:'尝试 #',
			ar:'# المحاولة',
			he:'ניסיון #',//
		},
		likeswitchuser: {
			en:'Would you like to switch users?',
			it:'Vuoi cambiare i giocatori?',
			fr:'Souhaites-tu échanger les utilisateurs ?',
			es:'¿Quieres cambiar de usuario?',
			pt:'Queres trocar de utilizador?',
			de:'Möchtest du die Nutzer wechseln?',
			pl:'Czy chcesz przełączyć użytkownika?',
			ru:'Сменить пользователя?',
			cn:'是否需要切换用户？',
			ar:'هل تريد تبيدل المستخدمين؟',
			he:'האם תרצו להחליף משתמשים?',
		},
		no: {
			upper:true,
			en:'no',
			it:'no',
			fr:'non',
			es:'No',
			pt:'Não',
			de:'Nein',
			pl:'Nie',
			ru:'Нет',
			cn:'否',
			ar:'لا',
			he:'לא',
		},
		yes: {
			upper:true,
			en:'yes',
			it:'sì',
			fr:'oui',
			es:'Sí',
			pt:'Sim',
			de:'Ja',
			pl:'Tak',
			ru:'Да',
			cn:'是',
			ar:'نعم',
			he:'כן',
		},
        playingas: {
			upper:true,
			en:'Playing as |NN|',
			it:'Stai giocando come |NN|',
			fr:'Joue en tant que |NN|',
			es:'Juega como |NN|',
			pt:'A jogar como |NN|',
			de:'Spielt als |NN|',
			pl:'Grasz jako |NN|',
			ru:'Играть как |NN|',
			cn:'以 |NN| 的身份参加游戏',
			ar:'اللاعب كـ |NN|',
			he:'משחק כ-|NN|',
		},
        selectplayer: {
			upper:true,
			en:'Select Player',
			it:'Seleziona il giocatore',
			fr:'Sélection du joueur',
			es:'Selecciona el jugador',
			pt:'Seleciona o jogador',
			de:'Spieler auswählen',
			pl:'Wybierz gracza',
			ru:'Выбери игрока',
			cn:'选择玩家',
			ar:'تحديد اللاعب',
			he:'בחרו שחקן',
		},
        loading:{
            upper:true,
			en:'Loading',
			it:'Caricamento',
			fr:'En cours de chargement',
			es:'Cargando...',
			pt:'Carregando',
			de:'Lädt',
			pl:'Ładowanie',
			ru:'Загрузка',
			cn:'正在加载',
			ar:'تنزيل',
			he:'טוען',
        },
          toobad:{
            upper:true,
			en:'Too bad. Try again!',
			it:'Peccato. Riprova!',
			fr:'Dommage ! Essaie encore !',
			es:'¡Qué pena! ¡Inténtalo de nuevo!',
			pt:'Que pena. Tente de novo!',
			de:'Ach herrje. Versuch es nochmal!',
			pl:'Szkoda. Spróbuj ponownie!',
			ru:'Очень жаль. Попробуйте снова!',
			cn:'太遗憾了。再来一次吧！',
			ar:'سيء جدًا. حاول مجددًا!',
			he:'חבל. נסו שוב!',
        },
        cancel:
        {
            upper:true,
            en:'Cancel',
			it:'Annulla',
			fr:'Annuler',
			es:'Cancelar',
			pt:'Cancelar',
			de:'Abbrechen',
			pl:'Anuluj',
			ru:'Отменить',
			cn:'取消',
			ar:'إلغاء',
			he:'ביטול',
        }
	},
	    availableLanguages:['en', 'it', 'fr', 'es', 'pt', 'de', 'pl', 'ru', 'ar', 'cn', 'he'],
    switchLanguage:function(){
        var nextLanguage = '';
        for(var i =0; i< this.availableLanguages.length; i++)
        {
            if(this.code == this.availableLanguages[i])
            {
                i == this.availableLanguages.length - 1 ? nextLanguage = this.availableLanguages[0] : nextLanguage = this.availableLanguages[i+1];
            }
        }

        window.location = 'index.html?lang='+nextLanguage;
    },
	languageController: function(code){
		if(this.fonts[code] != undefined)
        {
            this.code = code;
            console.log("[KOKO DEBUG] Language Found")
        }
        else 
        {
            console.log("[KOKO DEBUG] Use Default Language")
            this.code = 'en';
        }
	},
	getFontSource: function(){
		return this.fonts[this.code].src;
	},
	getTerm: function(term) {
        
        var strTerm = ''
        if(!this.terms[term].skipReverse)
        {
            strTerm = this.reverseString(this.terms[term][this.code]);
        }
        else    
        {
           strTerm = this.terms[term][this.code]; 
        }
        
		if(this.fonts[this.code].upper && this.terms[term].upper) {
			return strTerm.toUpperCase();
		} 
		return strTerm;
	},
    
    textType: function()
    {
        if(!this.fonts[this.code].alwaysUse)
        {
            return 'text' ;  
        }
        return 'bmptext';
    },
    getFontLTR: function(){
		return this.fonts[this.code].ltr;
	},
	getFontName: function($checkIgnore){
       
        if($checkIgnore && !this.fonts[this.code].alwaysUse)
        {
          return '';   
        }
        //if(this.fonts[this.code].name
		return this.fonts[this.code].name;
	},
    reverseString: function($str)
    {

        if(this.code == 'ar' || this.code == 'he')
        {
            $str = $str.split("").reverse().join("");
        }
        
        return $str;
    },
    hasChars:function($string)
    {
        var charsExist = true
        var font = lang.getFontName(false)
        for(var i=0; i<$string.length; i++)
        {
            var code = PIXI.extras.BitmapText.fonts[font].chars[$string.charCodeAt(i)]  
            if(code == undefined) charsExist = false
        }
        return charsExist;            
    },	
});
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}
var urlvars = getUrlVars();
var lang //= new languageController('en');