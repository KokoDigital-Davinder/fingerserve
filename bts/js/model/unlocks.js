var UNLOCKABLES = {
	calculate: function(){
		var unlocked = false;
		for( var i = 0; i < this.chars.length; i++ ){
			if(this.chars[i].stars <= user.totalstars) {
                
                if(this.chars[i].locked) unlocked = i;
				this.chars[i].locked = false;
				
			}
            else 
            {
              this.chars[i].locked = true;  
            }
		}
		return unlocked;
	},
	chars: [
		{
			name:'blue',
			stars: 0,
			locked: true,
			spriteName:'HandBlue'
		},
		{
			name:'green',
			stars:	0,
			locked: true,
			spriteName:'HandGreen'
		},
		{
			name:'orange',
			stars:	20,
			locked: true,
			spriteName:'HandOrange'
		},
		{
			name:'red',
			stars:	40,
			locked: true,
			spriteName:'HandRed'
		},
	]
}
UNLOCKABLES.calculate();
