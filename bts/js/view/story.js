koko.class('StoryView', 'view', {
    StoryView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
        
        this.children = koko.display.buildLayout([
			{ name:'bg_blurred', type:'sprite', id:'BackgroundBlurred', scale:4 },

			{ name:'story_container', type:'container' },
				{ name:'story_1_cont', type:'container', x:116, y:94, parent:'story_container' },
				{ name:'story1', type:'sprite', id:'StoryPanel1', parent:'story_1_cont' },

				{ name:'story_2_cont', type:'container', x:500, y:94, parent:'story_container' },
				{ name:'story2', type:'sprite', id:'StoryPanel2', parent:'story_2_cont' },

				{ name:'story_3_cont', type:'container', x:116, y:372, parent:'story_container' },
				{ name:'story3', type:'sprite', id:'StoryPanel3', parent:'story_3_cont' },

				{ name:'story_4_cont', type:'container', x:479, y:347, parent:'story_container' },
				{ name:'story4', type:'sprite', id:'StoryPanel4', parent:'story_4_cont' },

				{ name:'next_btn', type:'sprite', id:'StoryNextButton', x:0, y:0, regX:59, regY:59 },
        ], this.container);
        		
        var next_btn = new Button(this.children.next_btn, { scale: 1.1 }, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut();
			}.bind(this)
		});
        
        this.elements = [ next_btn ];
    },
	resize: function(){
		var vp = app.viewPort, c = this.children;
		
		c.next_btn.x = vp.right - 59 - 10;
		c.next_btn.y = vp.bottom - 59 - 10;
	},
    transitionIn: function(){
		
        this.buildView();
        this.show();
        this.enable();
		
		v.get('menuhud').setState('menus');
		
		
		d.applyThenAnimate(this.container, { alpha:0 }, .3, { alpha:1 });
		
		d.applyThenAnimate(this.children.story1, { alpha: 0, x: -200 }, .3, { onStart:function() { soundManager.playPopupSound()}, x:0, alpha:1, delay:0.5-0.3 });
		d.applyThenAnimate(this.children.story2, { alpha: 0, x: +200 }, .3, { onStart:function() { soundManager.playPopupSound()}, x:0, alpha:1, delay:1.0-0.3 });
		d.applyThenAnimate(this.children.story3, { alpha: 0, x: -200 }, .3, { onStart:function() { soundManager.playPopupSound()}, x:0, alpha:1, delay:1.5-0.3 });
		d.applyThenAnimate(this.children.story4, { alpha: 0, x: +200 }, .3, { onStart:function() { soundManager.playPopupSound()}, x:0, alpha:1, delay:2.0-0.3 });
		
		d.applyThenAnimate(this.children.next_btn, { scale:0 }, .3, { scale:1, ease:Back.easeOut, delay:2.5-0.3 });

		
		app.addResizeListener('story', this.resize.bind(this));
    },
    transitionOut: function(){
        this.disable();
		
		app.removeResizeListener('story');
		
		d.animateObject(this.container, .2, { alpha: 0, onComplete: function(){
			
			v.get('ingame').transitionIn();

			this.destroyView();  
			this.hide();
			
		}.bind(this)});
		
    }
});