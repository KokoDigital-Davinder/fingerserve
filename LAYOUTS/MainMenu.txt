[
{ name:'MainMenuContainer', type:'container', id:'MainMenuContainer', x:0, y:0 },
{ name:'Background', type:'sprite', id:'Background', parent:'MainMenuContainer', x:0, y:0 },
{ name:'GameLogo', type:'sprite', id:'GameLogo', parent:'MainMenuContainer', x:568, y:300, regX:321, regY:199 },
{ name:'CloseButton', type:'sprite', id:'CloseButton', parent:'MainMenuContainer', x:128, y:72, regX:45, regY:45 },
{ name:'AudioButton', type:'container', id:'AudioButton', parent:'MainMenuContainer', x:127, y:698 },
{ name:'SoundOn', type:'sprite', id:'SoundOn', parent:'AudioButton', x:0, y:0, regX:45, regY:45 },
{ name:'SoundOff', type:'sprite', id:'SoundOff', parent:'AudioButton', x:0, y:0, visible:false, regX:45, regY:45 },
{ name:'HighscoresButton', type:'sprite', id:'HighscoresButton', parent:'MainMenuContainer', x:1008, y:698, regX:45, regY:45 },
{ name:'PlayButton', type:'sprite', id:'PlayButton', parent:'MainMenuContainer', x:567, y:607, regX:105, regY:105 },
{ name:'SwitchUserButton', type:'sprite', id:'SwitchUserButton', parent:'MainMenuContainer', x:220, y:72, regX:45, regY:45 },
{ name:'StarsTotal', type:'container', id:'StarsTotal', parent:'MainMenuContainer', x:1008, y:74 },
{ name:'StarsTotalBack', type:'sprite', id:'StarsTotalBack', parent:'StarsTotal', x:-1, y:-1, regX:40, regY:41 },
{ name:'StarsTotalTextfield', type:'bmptext', parent:'StarsTotal', x:-4, y:-3, text:'22', size:'32px', font:'Londrina Solid', lw:73, tint:0xffffff, regX:37, regY:20 }
]