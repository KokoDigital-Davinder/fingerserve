koko.class('EndGameAnimView', 'view', {
   EndGameAnimView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
        this.children = koko.display.buildLayout([
            { name:'MainMenuContainer', type:'container', id:'MainMenuContainer', x:0, y:0 },
            { name:'Background', type:'sprite', id:'bg_gradient', parent:'MainMenuContainer', x:0, y:0 }, 
            { name:'Starburst', type:'sprite', id:'starburst', parent:'MainMenuContainer', x:0, y:0, regX:352,regY:353 }, 
            { name:'Hand_blue', type:'sprite', id:'end_hand_blue', parent:'MainMenuContainer', x:0, y:0,   regX:253,regY:366, visible:false}, 
            { name:'Hand_green', type:'sprite', id:'end_hand_green', parent:'MainMenuContainer', x:0, y:0,   regX:253,regY:366,visible:false}, 
            { name:'Hand_orange', type:'sprite', id:'end_hand_orange', parent:'MainMenuContainer', x:0, y:0,   regX:253,regY:366,visible:false}, 
            { name:'Hand_red', type:'sprite', id:'end_hand_red', parent:'MainMenuContainer', x:0, y:0,   regX:253,regY:366,visible:false}, 
		], this.container);	

    },
	
    transitionIn: function(){
        this.buildView();
        this.show();
        this.enable();
        this.container.alpha = 1;
        var t = this, g = app.guiSize, vp = app.viewPort;
        var colour = ['blue', 'green', 'orange', 'red'][user.char ? user.char : 0];
            
        this.children.Starburst.x = this.children['Hand_'+colour].x = vp.centerX;
        this.children.Starburst.y = this.children['Hand_'+colour].y = vp.centerY;  
    
        this.children.Starburst.scale.x = this.children.Starburst.scale.y =  this.children['Hand_'+colour].scale.x = this.children['Hand_'+colour].scale.y = 0;
        this.children['Hand_'+colour].visible = true; 
        this.children['Hand_'+colour].rotation = -30; 
     
        var delay = .2; 
        
        d.animateObject(this.children.Starburst, 5, {rotation:361,repeat:-1,ease:Linear.easeNone}); 
        d.animateObject(this.children.Starburst, .3, { delay:delay, scale:1, ease:Back.easeOut}); 
        delay = .5;
        d.animateObject(this.children['Hand_'+colour], 1, { delay:delay, rotation:0, scale:0.9, ease:Back.easeBack}); 
 
        setTimeout(function(){t.transitionOut()}, 3500); 
        soundManager.playGameWonSound();

		app.addResizeListener('endgameanim', this.resize.bind(this));
    },
    transitionOut: function(){
		d.animateObject(this.container, .3, { alpha:0, onComplete: function(){
			this.destroyView();  
			this.hide();
			app.removeResizeListener('endgameanim');
            
            var unlocked = UNLOCKABLES.calculate();
		
            if(unlocked !== false) 
            {
                to = 'gameover';
                v.get('newchar').transitionIn(to, unlocked);	
            }
            else
            {
                v.get('gameover').transitionIn();
            }
            
             
		}.bind(this)});
    }
});