// JavaScript Document
var main = {
	init: function(){
        
		main.setupViews();
                   
		main.setupAudio();
		main.setupComplete();
	},
	setupViews: function(){
		v.add('preloader',       new PreloaderView(),               app.stage);
		
		v.add('background',       new BackgroundView(),               	app.stage);
		v.add('mainmenu',         new MainMenuView(),                 	app.stage);
		v.add('charselect',       new CharacterSelectView(),            app.stage);
		v.add('story',       	  new StoryView(),             	 		app.stage);
		v.add('ingame',       	  new InGameView(),             	 	app.stage);
		v.add('ingamehud',        new InGameHUDView(),             	 	app.stage);
		v.add('highscores',       new HighScoresView(),             	app.stage);
		v.add('gameover',     	  new GameOverView(),             		app.stage);
        v.add('endgameanim',      new EndGameAnimView(),             	app.stage);
		v.add('menuhud',       	  new MenuHUDView(),               		app.stage);
		v.add('howtoplay',        new HowToPlayView(),             	 	app.stage);
		v.add('paused',       	  new PausedGameView(),               	app.stage);
		v.add('switchuser',       new SwitchUserView(),               	app.stage);
		v.add('newchar',     	  new NewCharView(),               		app.stage);
		v.add('userloggedin',     new UserChangedView(),               	app.stage);
        v.add('selectuser',       new SelectUserView(),                 app.stage);
        
        //initiaiteKokoConsole();
       
		
    },
	setupAudio: function() {
		au.dir = 'audio/';
        
		au.addSoundGroup('music', 1);
		au.addSoundGroup('sfx',   1);

		var fileExt = '.mp3';
		
        if(kokodebug)
        {
            au.add('pop1',   'pop1' + fileExt,        1,      'sfx');
            au.add('pop2',   'pop2' + fileExt,        1,      'sfx');
            au.add('pop3',   'pop3' + fileExt,        1,      'sfx');
            au.add('pop4',   'pop4' + fileExt,        1,      'sfx');
            au.add('pop5',   'pop5' + fileExt,        1,      'sfx');
            au.add('whoosh1',   'whoosh1' + fileExt,        1,      'sfx');
            au.add('whoosh2',   'whoosh2' + fileExt,        1,      'sfx');
            au.add('whoosh3',   'whoosh3' + fileExt,        1,      'sfx');
            au.add('whoosh4',   'whoosh4' + fileExt,        1,      'sfx');
            au.add('notification1',   'notification1' + fileExt,        1,      'sfx');
            au.add('error1',   'error1' + fileExt,        1,      'sfx');
            au.add('star',   'star' + fileExt,        1,      'sfx');
            au.add('victory',   'victory' + fileExt,        1,      'sfx');
            au.add('lose',   'lose' + fileExt,        1,      'sfx');
            au.add('success4',   'success4' + fileExt,        1,      'sfx');
            au.add('bounce',   'bounce' + fileExt,        1,      'sfx');
            au.add('hit1',   'hit1' + fileExt,        1,      'sfx');
            au.add('hit2',   'hit2' + fileExt,        1,      'sfx');
            au.add('boing1',   'boing1' + fileExt,        1,      'sfx');
            au.add('error1',   'error1' + fileExt,        1,      'sfx');
            au.add('success1',   'success1' + fileExt,        1,      'sfx');
            au.add('whoosh1',   'whoosh1' + fileExt,        1,      'sfx');
            au.add('whoosh2',  'whoosh2' + fileExt,        1,      'sfx');
            au.add('whoosh3',  'whoosh3' + fileExt,        1,      'sfx');
            au.add('whoosh4',  'whoosh4' + fileExt,        1,      'sfx');
            au.add('collect',  'collect' + fileExt,        1,      'sfx');
            au.add('boing7',   'boing7' + fileExt,        1,      'sfx');
           
            au.add('splash1',  'splash1' + fileExt,        1,      'sfx');
            
            au.add('hit1',  'hit1' + fileExt,        1,      'sfx');
            au.add('hit2',  'hit2' + fileExt,        1,      'sfx');
            au.add('boing8',   'boing8' + fileExt,        1,      'sfx');
            au.add('boing7',  'boing7' + fileExt,        1,      'sfx');
            au.add('boing6',   'boing6' + fileExt,        1,      'sfx');
            au.add('boing5',   'boing6' + fileExt,        1,      'sfx');
            au.add('boing3',   'boing3' + fileExt,        1,      'sfx');
            au.add('boing4',  'boing4' + fileExt,        1,      'sfx');
            au.add('success2',  'success2' + fileExt,        1,      'sfx');
            au.add('background', 'background' + fileExt,    1,      'sfx');
        }
	},
	soundsLoaded: false,
	graphicsLoaded:false,
    setupComplete: function(){
		//load the preloader graphics, before showing anything
		 user.load();
	},
    userLoaded: function()
    {
        v.get('preloader').transitionIn();
        a.manifest = [
            { id:'preloader', src:'img/preloader.json' },
            { id:'londrina_font', src:lang.getFontSource() }
        ];
        a.loadManifest(function(p){
            v.get('preloader').updateGFXPerc(p);
        }, function() {
            main.preloaderLoaded();
		});  
            
    },
	 preloaderLoaded: function()
    {
         v.get('preloader').setupProperGraphics();
		//load the preloader graphics, before showing anything
		a.manifest = [
			{ id:'backgrounds_json', src:'img/background.json' },
			{ id:'characters_json', src:'img/characters.json' },
			{ id:'menus_json', src:'img/menus.json' },
			{ id:'buttons_json', src:'img/buttons.json' },
			{ id:'ingame_json', src:'img/ingame.json' },
			{ id:'logo_json', src:'img/logo.json' },
			{ id:'starburst_json', src:'img/starburst.json' },
			{ id:'font', src:lang.getFontSource() },
			{ id:'finger_json', src:'img/rackets.json'},
            { id:'endscreen_json', src:'img/endscreen.json'}
		];
		
		//load sounds
        if(kokodebug)
        {
             var totalLoaded = 0;
            au.loadedHandler = function(){
                this.soundsLoaded = true;
                soundManager.playBackgroundSound(); 

            }.bind(this);
            au.soundLoaded = function(){
                v.get('preloader').updateSFXPerc(++totalLoaded/38);
            }
            au.loadSounds();
        }
        else
        {
            koko.views.get('preloader').updateSFXPerc(1);     
        }    
		
		//load graphics
        a.loadManifest(function(p){
            v.get('preloader').updateGFXPerc(p);
        }, function() {
             v.get('preloader').updateGFXPerc(1);
		});   
		/*a.loadManifest(function(p){
			
		}, function() {	
            this.graphicsLoaded = true;
			this.hasLoaded();
		}.bind(this));*/
	},
	hasLoaded: function() {
         user.buildPlayerList(avatar_list)
       this.gotoApp();
		//if(this.graphicsLoaded && this.soundsLoaded) this.gotoApp();
	},
    preloadComplete: function(){
        user.buildPlayerList(avatar_list)
       this.gotoApp();
    },
	gotoApp: function(){
        
         setTimeout( function(){soundManager.playBackgroundSound()}, 1000)
		if(true) {
        //user.load();
			v.each(['background', 'menuhud', 'mainmenu'], 'transitionIn');
            
		} else{
		//	v.get('ingamehud').hasShownHowToPlay = true;
			//game.setupDefaultObstacles();
			v.each(['background', 'menuhud', 'ingame'], 'transitionIn');
		}
	}
    	/*gotoApp: function(){
		if(true) {
            user.load();
            soundManager.playBackgroundSound(); 
			v.each(['background', 'menuhud', 'mainmenu'], 'transitionIn');
		} else{
			v.get('ingamehud').hasShownHowToPlay = true;
			game.setupDog(1);
			//game.setupDefaultObstacles();
			v.each(['background', 'menuhud', 'ingame'], 'transitionIn');
		}
	}*/
}
