koko.class('MenuHUDView', 'view', {
    MenuHUDView: function(){
        this.superr.view.call(this);
    },
      isButtonLocked:false,
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
        
        this.children = koko.display.buildLayout([
            { name:'close_btn', type:'sprite', id:'CloseButton', x:128, y:72, regX:45, regY:45, visible:false },
			{ name:'switch_user_btn', type:'sprite', id:'SwitchUserButton', x:220, y:72, regX:45, regY:45, visible:false },
			{ name:'pause_btn', type:'sprite', id:'PauseButton', regX:45, regY:45, visible:false },
			
			{ name:'back_btn', type:'sprite', id:'PreviousButton', regX:45, regY:45, visible:false },
			
			{ name:'audio_btn', type:'container', x:127, y:698, visible:false },
			{ name:'audio_on', type:'sprite', id:'SoundOn', parent:'audio_btn', x:0, y:0, regX:45, regY:45 },
			{ name:'audio_off', type:'sprite', id:'SoundOff', parent:'audio_btn', x:0, y:0, visible:false, regX:45, regY:45 },
			
			
			{ name:'highscores_btn', type:'sprite', id:'HighscoresButton', x:1008, y:698, regX:45, regY:45, visible:false },
			
			{ name:'stars_container', type:'container', x:1008, y:74 },
			{ name:'stars_back', type:'sprite', id:'StarsTotalBack', parent:'stars_container', regX:80 },
			{ name:'stars_txt', type:'bmptext', parent:'stars_container', text:user.stars.toString(), size:'32px', font:lang.getFontName(), tint:0xffffff, x: -43, y: 22, align:'center' }
			
		], this.container);
		
		
		var close_btn = new Button(this.children.close_btn, {scale:1.1}, {
			click: function()
            {
                if(!this.isButtonLocked){
                    this.lockButton();
                location.href = 'http://contentpostroll/';
                     }
			}.bind(this)
		});
		
		var audio_btn = new Button(this.children.audio_btn, {scale:1.1}, {
			click: function(){
                if(!this.isButtonLocked){
                    this.lockButton();
               // soundManager.playButtonSound();
                soundManager.muteOrUnmute(); 
                
				au.muted = !au.muted;
				this.children.audio_on.visible = !au.muted;
				this.children.audio_off.visible = au.muted;
				user.saveSettings();
                     }
			}.bind(this)
		});
		
		var highscores_btn = new Button(this.children.highscores_btn, {scale:1.1}, {
			click: function(){
                if(!this.isButtonLocked){
                    this.lockButton();
                soundManager.playButtonSound();
				if(v.get('mainmenu').container.visible) {
					v.get('highscores').exitMenu = 'mainmenu';
					v.get('highscores').transitionIn();
					v.get('mainmenu').transitionOut();
				}
				else if(v.get('gameover').container.visible) {
					v.get('gameover').transitionOut();
					v.get('highscores').exitMenu = 'gameover';
					v.get('gameover').exitThisMenu('highscores');
				}
                     }
			}.bind(this)
		});
		
		var switch_user_btn = new Button(this.children.switch_user_btn, {scale:1.1}, {
			click: function(){
                if(!this.isButtonLocked){
                    this.lockButton();
                soundManager.playButtonSound();
				v.get('switchuser').transitionIn();
                     }
			}.bind(this)
		});
		
		var back_btn = new Button(this.children.back_btn, {scale:1.1}, {
			click: function(){
                if(!this.isButtonLocked){
                    this.lockButton();
                    soundManager.playButtonSound();
                    if(v.get('charselect').container.visible) {
                        v.get('charselect').transitionOut('mainmenu');
                    } else {
                        v.get('highscores').transitionOut();
                    }
                }
			}.bind(this)
		});
		
		var pause_btn = new Button(this.children.pause_btn, {scale:1.1}, {
			click: function(){
                if(!this.isButtonLocked){
                    this.lockButton();
                soundManager.playButtonSound();
				game.paused = true;
				v.get('paused').transitionIn();
                }
			}.bind(this)
		});
       
        this.elements = [close_btn, audio_btn, highscores_btn, switch_user_btn, pause_btn, back_btn ];
    },
	resize: function(){
		var c = this.children, g = app.guiSize, vp = app.viewPort;
		
		c.close_btn.x = vp.left + 45 + 10;
		c.close_btn.y = vp.top + 45 + 10;
	
		c.pause_btn.x = c.switch_user_btn.x = c.close_btn.x + 90 + 10;
		c.pause_btn.y = c.switch_user_btn.y = c.close_btn.y;
		
		c.back_btn.x = c.audio_btn.x = c.close_btn.x;
		c.back_btn.y = c.audio_btn.y = vp.bottom - 45 - 10;
		
		c.highscores_btn.x = vp.right - 45 - 10;
		c.highscores_btn.y = vp.bottom - 45 - 10;
		
		c.stars_container.x = vp.right -10;
		c.stars_container.y = vp.top + 10;
	},
    transitionIn: function(){
        this.buildView();
        this.show();
        this.enable();
		
		for( var i = 0; i < this.container.children.length; i++ ){
			this.container.children[i].visible = false;	
		}
		
		app.addResizeListener('menuhud', this.resize.bind(this));
    },
      lockButton:function()
    {
        this.isButtonLocked = true;
        setTimeout(function(){this.isButtonLocked = false}.bind(this), 500)
    },
	setState: function(state) {
		
		for( var i = 0; i < this.container.children.length; i++ ){
			this.container.children[i].visible = false;	
		}
		
		this.children.close_btn.visible = true;
		this.children.stars_container.visible = true;
		this.children.stars_txt.text = user.totalstars;
		
		switch( state ) {
			case 'mainmenu':
				this.children.audio_btn.visible = true;
				this.children.switch_user_btn.visible = true;
				this.children.highscores_btn.visible = true;
			break
			case 'menus':
				this.children.close_btn.visible = true;
			break;
			case 'ingame':
				this.children.stars_container.visible = false;
				this.children.pause_btn.visible = true;
			break;
			case 'highscores':
				this.children.back_btn.visible = true;
			break;
            case 'nobuttons':                
             	for( var i = 0; i < this.container.children.length; i++ ){
                    this.container.children[i].visible = false;	
                }   
            break;
		}
	},
	showBackButton: function(){
		this.children.back_btn.visible = true;
	},
    transitionOut: function(){
        this.stopGame();
        this.destroyView();  
        this.hide();
        app.removeResizeListener('menuhud');
    }
});