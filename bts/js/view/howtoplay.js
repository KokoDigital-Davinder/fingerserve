koko.class('HowToPlayView', 'view', {
    HowToPlayView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
        this.children = koko.display.buildLayout([
			
			
			{ name:'bg', type:'shape', alpha:0.7, w:g.width, h:g.height},
			
			{ name:'panel_cont', type:'container', id:'HTPPanel', x:568, y:230 },
			{ name:'panel_bg', type:'sprite', id:'HTPBack1b', parent:'panel_cont', x:0, y:-10, regX:287 },
			
			{ name:'title_cont', type:'container', parent:'panel_cont', s:0.79, y: -90 },
			{ name:'title_bg', type:'sprite', id:'WFTitle', parent:'title_cont', x:0, y:0, regX:238 },
			{ name:'title_txt', type:'bmptext', parent:'title_cont', fitWidth:315, text:lang.getTerm('howtoplay'), size:'45px', font:lang.getFontName(), tint:0xf24321, align:'center', y: 15 },
			
			{ name:'slide_1', parent:'panel_cont' },
				{ name:'bone1', type:'sprite', id:'ball', parent:'slide_1', x:-75, y:30, regX:40, regY:40, rotation:180, scale:0.6, },
				{ name:'s1_hand', type:'sprite', id:'HTPHand', parent:'slide_1', x:140, y:120, scale:0.7, alpha:0.4 },
				
			{ name:'wind_cont', type:'container', x:100, y:15, parent:'slide_1', scale:0.7 },
					{ name:'wind_bg', type:'sprite', id:'WindSpeedBack', parent:'wind_cont' },
					{ name:'wind_title', type:'bmptext', parent:'wind_cont', x:85, y:10, fitWidth:120, text:lang.getTerm('windspeed'), size:'22px', font:lang.getFontName(),tint:0xf24321 },
					{ name:'wind_speed_txt', type:'bmptext', parent:'wind_cont', x:85, y:30, text:'+2', size:'46px', font:lang.getFontName(), tint:0x999999 },
					{ name:'windmph_title', type:'bmptext', text:lang.getTerm('mph'), size:'28px', fitWidth:70, font:lang.getFontName(), tint:0x999999, parent:'wind_cont', x:140, y:43, },
					{ name:'wind_arrows', type:'container', id:'WindArrows', parent:'wind_cont', x:41, y:41, regX:31, regY:31 },
					{ name:'wind_arrow_good', type:'sprite', id:'WindArrowGood', parent:'wind_arrows' },

			{ name:'slide_2', parent:'panel_cont' },
				{ name:'htp_trampoline', type:'sprite', id:'bst_springnet1', parent:'slide_2', x:-92, y:140, scale:0.6 },
				{ name:'bone2', type:'sprite', id:'ball', parent:'slide_2', x:-200, y:30, regX:40, regY:40, rotation:180, scale:0.6, },
						
			{ name:'slide_3', parent:'panel_cont' },
				{ name:'htp_fence', type:'sprite', id:'obs_umpirechair', parent:'slide_3', y: 45, x:50, scale:0.6 },
				{ name:'bone3', type:'sprite', id:'ball', parent:'slide_3', x:-200, y:30, regX:40, regY:40, rotation:180, scale:0.6, },

			{ name:'slide_4', parent:'panel_cont' },
				{ name:'bone4', type:'sprite', id:'ball', parent:'slide_4', x:-300, y:150, regX:40, regY:40, rotation:180, scale:0.6, },
				{ name:'collect_star_panel', type:'container', regY:271, scale:0.5, parent:'slide_4', y: 250 },
				{ name:'collectible_tab', type:'sprite', id:'collectible_tab', parent:'collect_star_panel', regX:132, y: 30 },
				{ name:'collectible_sunburst', type:'sprite', id:'collectible_sunburst', parent:'collect_star_panel', regX:157, regY:158, y:165, x:5 },
				{ name:'collectible_star', type:'sprite', id:'collectible_star', parent:'collect_star_panel', regX:64, regY:65, y:165, x: 5 },
				{ name:'s4_hand', type:'sprite', id:'HTPHand', parent:'slide_4', x:180, y:145, alpha:0.4, scaleY:0.7, scaleX:-0.7 },
			
			{ name:'htp_pippets', type:'container', parent:'panel_cont', y: 295, regX:50 },
			{ name:'htp_pippet_inactive1', type:'sprite', id:'htp_pippet_inactive', parent:'htp_pippets', x:0, y:0, regX:8, regY:8 },
			{ name:'htp_pippet_inactive2', type:'sprite', id:'htp_pippet_inactive', parent:'htp_pippets', x:25, y:0, regX:8, regY:8 },
			{ name:'htp_pippet_inactive3', type:'sprite', id:'htp_pippet_inactive', parent:'htp_pippets', x:50, y:0, regX:8, regY:8 },
			{ name:'htp_pippet_inactive4', type:'sprite', id:'htp_pippet_inactive', parent:'htp_pippets', x:75, y:0, regX:8, regY:8 },
			{ name:'htp_pippet_active', type:'sprite', id:'htp_pippet_active', parent:'htp_pippets', x:0, y:0, regX:8, regY:8 },
			
			{ name:'next_btn', type:'sprite', id:'NextButton', x:g.width-180, y:vp.centerY, regX:45, regY:45 },
			{ name:'prev_btn', type:'sprite', id:'PreviousButton', x:180, y:vp.centerY, regX:45, regY:45 },
			
			{ name:'skip_btn', type:'sprite', id:'HowToPlaySkipButton', regX:59, regY:59 },
		], this.container);
		
		
		this.bat = new FingerView();
		this.children.slide_1.addChildAt(this.bat.container,0);
		this.bat.transitionIn();
		d.applyProps(this.bat.container, { y: 50, scale:0.6 });

		
		
		d.mask(this.children.slide_1, -260, 10, 520, 255);
		d.mask(this.children.slide_2, -260, 10, 520, 255);
		d.mask(this.children.slide_3, -260, 10, 520, 255);
		d.mask(this.children.slide_4, -260, 10, 520, 255);
		
		var next_btn = new Button(this.children.next_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.autoPlay = false;
				this.nextSlide();
			}.bind(this)
		});
		
		var prev_btn = new Button(this.children.prev_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.autoPlay = false;
				this.prevSlide();
			}.bind(this)
		});

		var skip_btn = new Button(this.children.skip_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut();
			}.bind(this)
		});
        
        this.elements = [ next_btn, prev_btn, skip_btn ];
        this.swipeGestureSetup();
    },
    initialPoint:null,
    finalPoint:null,
    swipeGestureSetup:function()
    {
        var target = this.children.panel_cont;
        var t = this;
        target.touchstart = function (interactionData) 
        {
           initialPoint = interactionData.data.getLocalPosition(target.parent);
        }
 
        target.touchend = target.touchendoutside = function (interactionData) {
           finalPoint = interactionData.data.getLocalPosition(target.parent);
           var xAbs = Math.abs(initialPoint.x - finalPoint.x);
           var yAbs = Math.abs(initialPoint.y - finalPoint.y);
           if (xAbs > 20 || yAbs > 20) 
           {
               //check if distance between two points is greater then 20 otherwise discard swap event
               if (xAbs > yAbs) 
               {
                   if (finalPoint.x < initialPoint.x)
                   {
                       t.nextSlide();
                       //console.log("swap left");
                   }
                   else
                   {
                       t.prevSlide();
                       //console.log("swap right");
                   }
               }
               else 
               {
                   if (finalPoint.y < initialPoint.y)
                   {
                        t.prevSlide();
                        //console.log("swap up");
                   }
                   else
                   {
                       t.nextSlide();
                       //console.log("swap down");
                   }
               }
           }
       }
        target.interactive = true; 
    },
    
	currentSlide:0,
	totalSlides: 4,
	nextSlide: function(){
		if(this.currentSlide == (this.totalSlides-1)) {
			this.currentSlide = this.totalSlides - 2;	
		}
		
		this.currentSlide++;
		this.killCurrentFrame();
		this.gotoSlide();
	},
	prevSlide: function(){
		if(this.currentSlide == 0) return;
		
		this.currentSlide--;
		this.killCurrentFrame();
		this.gotoSlide();
	},
	killCurrentFrame: function(){
		clearTimeout(this.nextSlideTimeout);
		//destroy all tweens
		for( var key in this.children){
			TweenMax.killTweensOf(this.children[key]);	
		}
		
		TweenMax.killTweensOf(this.objr);	
	},
	gotoSlide: function(){
		this.children.htp_pippet_active.x = 25 * this.currentSlide;
		
		for( var i =0; i < this.totalSlides; i++ ){
			this.children['slide_'+(i+1)].visible = false;
		}
		this.children['slide_'+(this.currentSlide+1)].visible = true;
		this.animateSlide();
	},
	autoPlay: true,
	nextSlideTimeout: null,
	animationComplete:function(){
		if(this.autoPlay) {
			this.nextSlide(); 
		} else {
			this.nextSlideTimeout = setTimeout(function(){ this.gotoSlide(); }.bind(this), 500);
		}
	},
	animateSlide: function(){
		switch(this.currentSlide) {	
			case 0:
				this.children.slide_1.setChildIndex(this.bat.container, 0);
				this.bat.setIdle();
				
				
				this.objr = { windSpeed: 2 };
				TweenLite.to(this.objr, 2, { windSpeed:10, ease:Linear.easeNone, onUpdate: function(){
					this.children.wind_speed_txt.text = '+'+Math.round(this.objr.windSpeed);
				}.bind(this)});
				
				d.animateObject(this.children.bone1, 3, { rotation:600});
				d.applyProps( this.children.bone1, { y:50, x:-60 });
				d.applyProps( this.children.slide_1, { alpha:0 });
				d.animateObject( this.children.slide_1, .2, { alpha:1 });
				
				d.animateObject( this.children.bone1, .5,  { y: 150, ease:Sine.easeIn });
				d.animateObject( this.children.bone1, .3,  { delay:.5, y: 50, ease:Sine.easeOut });
				d.animateObject( this.children.bone1, .3,  { delay:.8, y: 150, ease:Sine.easeIn });
				d.animateObject( this.children.bone1, .7,  { delay:1.1, y: -200, ease:Sine.easeOut });
				d.animateObject( this.children.bone1, .5,  { delay:1.8, y: 130, ease:Sine.easeIn });
				d.animateObject( this.children.bone1, .5,  { delay:2.3, y: -100, x: 500, ease:Sine.easeIn });
				
				
				setTimeout(function(){
					if(this.bat) this.bat.playAnim('keepup');
				}.bind(this), 350);
				
				setTimeout(function(){
					if(this.bat) this.bat.playAnim('keepup');
				}.bind(this), 950);
				
				setTimeout(function(){
					if(!this.bat) return;
					this.children.slide_1.setChildIndex(this.bat.container, this.children.slide_1.children.length-1);
					this.bat.playAnim('hit');
				}.bind(this), 2050);
				
				
				d.animateObject( this.children.s1_hand, .5, { delay:1.3, alpha:1, x:90, y:70 });
				d.animateObject( this.children.s1_hand, .05, { scale:0.6, delay:1.8 });
				d.animateObject( this.children.s1_hand, .05, { scale:0.7, delay:1.9 });
				d.animateObject( this.children.s1_hand, .2, { alpha:0, delay:2.0 });
				d.animateObject( this.children.slide_1, .3, { alpha:0, delay:2.8, onComplete: function(){ 
					this.animationComplete();
				}.bind(this) })
				
				
			break;
			case 1:
				
				d.applyProps(this.children.slide_2, { alpha:0 });
				
				d.applyProps(this.children.bone2, { x:-200, y:30, rotation:180 }, true);
				
				d.animateObject(this.children.slide_2, .2, { alpha:1 });
				d.animateObject( this.children.bone2, .6, { delay:.2, x: 0, y: 180, rotation: 340, ease:Linear.easeNone });
				d.animateObject( this.children.bone2, .4, { delay: .8, x: 200, y: 30, rotation: 520, ease:Linear.easeNone });
				d.animateObject( this.children.slide_2, .3, { alpha:0, delay:1.1, onComplete: function(){ 
					this.animationComplete(); 
				}.bind(this)})
				
			break;
			case 2:
				
				d.applyProps(this.children.slide_3, { alpha:0 });
				d.applyProps(this.children.bone3, { x:-200, y:30, rotation:180 });
				
				d.animateObject( this.children.slide_3, .2, { alpha:1 });
				d.animateObject( this.children.bone3, .6, { delay:.2, x: 50, y: 180, rotation: 340, ease:Linear.easeNone });
				d.animateObject( this.children.bone3, .2, { delay: .8, x: 0, y: 200, rotation: 180, ease:Linear.easeNone });
				d.animateObject( this.children.slide_3, .3, { alpha:0, delay:1.3, onComplete: function(){ 
					this.animationComplete(); 
				}.bind(this) });
				
			break;
			case 3:

				d.applyProps(this.children.slide_4, { alpha:0 });
				d.applyProps(this.children.collect_star_panel, { visible:false});
				d.applyProps(this.children.bone4, {x:-300, y:100, rotation:180});
				d.applyProps(this.children.s4_hand, {x:180, y:145, alpha:0, scaleY:0.7, scaleX:-0.7});
				d.applyProps(this.children.collect_star_panel, { rotation:-90, x:260+100, y:120, alpha:0, visible: true });
				d.applyProps(this.children.collectible_star, { alpha:1, scale:1 });
				d.applyProps(this.children.collectible_sunburst, { scale:0, visible:false });
				d.applyProps(this.children.collectible_tab, { alpha:1 });
				
				
				d.animateObject( this.children.slide_4, .2, { alpha:1 });
								
				d.animateObject(this.children.bone4, .6, { y: 200, yoyo:true, repeat:4, ease:Quad.easeIn, onUpdate:function(){
					d.applyProps(this.children.bone4, { rotation: m.radiansToAngle(this.children.bone4.rotation)+5 });
				}.bind(this), onComplete: function(){  this.animationComplete();  }.bind(this)});
				
				d.animateObject(this.children.bone4, 2.4, { x: 300, ease:Linear.easeNone });
				d.animateObject(this.children.collect_star_panel, .2, { delay:0.2, x:260, alpha: 1 });
				d.animateObject( this.children.s4_hand, .5, { delay:.7, alpha:1, x:210, y:115 });
				d.animateObject( this.children.s4_hand, .05, { scaleY:0.6, scaleX:-0.6, delay:1.2, onComplete:function(){
					d.applyThenAnimate(this.children.collectible_sunburst, { visible:true, alpha:0, scale:0 }, .6, { alpha:1, scale:1, onUpdate:function(){
						d.apply(this.children.collectible_sunburst, { rotation: m.radiansToAngle(this.children.collectible_sunburst.rotation) + 10 });
						d.apply(this.children.collectible_star, { rotation: m.radiansToAngle(this.children.collectible_star.rotation) - 10 });
					}.bind(this)});

					d.animate(this.children.collectible_tab, .6,  { alpha:0.3 });

					d.animate(this.children.collectible_star, .4, { delay:0.05, alpha:0, scale:1.3});

					d.animate(this.children.collect_star_panel, .2, { delay: .4, alpha:0 });
				}.bind(this)});
				d.animateObject( this.children.s4_hand, .05, {  scaleY:0.7, scaleX:-0.7, delay:1.3 });
				d.animateObject( this.children.s4_hand, .2, { alpha:0, delay:1.8 });
			break;
		}	
	},
    resize: function() {
        var c = this.children, g = app.guiSize, vp = app.viewPort;
		
		d.applyProps(c.next_btn, { y: vp.centerY });
		d.applyProps(c.prev_btn, { y: vp.centerY });
		d.applyProps(c.skip_btn, { x: vp.right - 59 - 30, y: vp.bottom - 59 - 30 });
    },
    transitionIn: function(){
		this.buildView();
		this.show();
		
		v.all(function(v){
			if(v.container.visible) v.disable();
		});
		this.enable();
		
		
		d.applyThenAnimate(this.container, { alpha:0 }, .3, { alpha:1});
		
		//resize handler
		app.addResizeListener('howtoplay', this.resize.bind(this));
		
		//start
		this.autoPlay = true;
		this.gotoSlide();
    },
    transitionOut: function(){
		app.removeResizeListener('howtoplay');
		this.killCurrentFrame();
		clearTimeout(this.nextSlideTimeout);
		this.disable();
		
		
		d.animateObject(this.container, .3, { alpha:0, onComplete:function(){
			this.destroyView();  
			this.bat.transitionOut();
			this.bat = null;
			this.hide();
			v.get('ingamehud').transitionIn();
			
			v.all(function(v){
				if(v.container.visible) v.enable();
			});
		}.bind(this)});
    }
});