koko.class('MainMenuView', 'view', {
    MainMenuView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
        this.children = koko.display.buildLayout([
			{ name:'game_logo',x:568, y:100, regY:200 },            
            { name:'logo_shield', type:'sprite', id:'mainmenu_shield', regX:172, parent:'game_logo'},
            { name:'logo_ball', type:'sprite', id:'mainmenu_ball', y:115, regX:54,regY:54, parent:'game_logo'},
            { name:'logo_racket_left', type:'sprite', id:'mainmenu_racket_green',x:-11,y:257,regX:225,regY:225, parent:'game_logo'},
            { name:'logo_racket_right', type:'sprite', id:'mainmenu_racket_orange',x:11,y:257,regX:14,regY:225, parent:'game_logo'},
            
			{ name:'logo_txt', type:'sprite', id:lang.getTerm('logo_sprite_id'), parent:'game_logo', y:200 },
            
			{ name:'play_btn', type:'sprite', id:'PlayButton', x:567, y:607, regX:105, regY:105 },
		], this.container);
        
		this.children.logo_txt.pivot.x = this.children.logo_txt.width*0.5;
       
        var play_btn = new Button(this.children.play_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut('charselect');
			}.bind(this)
		});
        
        this.elements = [ play_btn ];
    },
    setConsoleText: function($txt){
        //this.children.logText.text = $txt;
    },
    resize: function() {
        var c = this.children, g = app.guiSize, vp = app.viewPort;
        
        c.play_btn.x = vp.centerX;
        c.play_btn.y = vp.centerY + 215;
		
		c.game_logo.x = vp.centerX;
		c.game_logo.y = vp.centerY - 110;
    },
    transitionIn: function(){
		this.buildView();
		this.show();
		this.enable();
        
		//resize handler
		app.addResizeListener('mainmenu', this.resize.bind(this));
        
		v.get('menuhud').setState('mainmenu');
        
        d.animateObject(this.children.logo_ball, 2, {rotation:360, repeat:-1, ease:Linear.easeNone}); 
        
		d.applyThenAnimate(this.container, { alpha:0 }, .5, { alpha:1 });
		d.applyThenAnimate(this.children.play_btn, { scale:0 }, .5, { ease:Back.easeOut, scale:1, delay:0.3 });
		d.applyThenAnimate(this.children.game_logo, { scale:0 }, .5, { ease:Back.easeOut, scale:1, delay:0.5 });
        
        d.applyThenAnimate( this.children.logo_racket_left, { rotation:-40 }, .5, { ease:Linear.easeNone, rotation:0, delay:0.5 });
        d.applyThenAnimate( this.children.logo_racket_right, { rotation:40 }, .5, { ease:Linear.easeNone, rotation:0, delay:0.5 }); 
		
    },
    transitionOut: function(menuOut){
        
		app.removeResizeListener('mainmenu');
		this.disable();
		
		d.animateObject(this.container, .2, { alpha:0, onComplete:function(){
			this.destroyView();  
        	this.hide();			
			if(menuOut) {
				if(menuOut == 'newchar') {
					v.get(menuOut).transitionIn('charselect');
				} else {
					v.get(menuOut).transitionIn();
				}
			}
		}.bind(this)});
		
    }
});