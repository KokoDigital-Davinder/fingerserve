koko.class('soundManager', null, {
    isDebug: kokodebug,
    isMuted: false, 
    soundManager: function () {},

    //Start generic sounds... 
    playButtonSound: function () {
        var soundName = "";
        var randomButtonClickSound = m.randomInt(0, 4);
        switch (randomButtonClickSound) {
        case 0:
            soundName = "pop1";
            break;
        case 1:
            soundName = "pop2";
            break;
        case 2:
            soundName = "pop3";
            break;
        case 3:
            soundName = "pop4";
            break;
        case 4:
            soundName = "pop5";
            break;
        }
        this.play(soundName);
    },

    playPopupSound: function () {
        var soundname = "";
        var randomButtonClickSound = m.randomInt(0, 3);
        switch (randomButtonClickSound) {
        case 0:
            soundName = "whoosh1";
            break;
        case 1:
            soundName = "whoosh2";
            break;
        case 2:
            soundName = "whoosh3";
            break;
        case 3:
            soundName = "whoosh4";
            break;
        }
        this.play(soundName);
    },

    playNotificationSound: function () {
        this.play("notification1");
    },

    playLockedSound: function () {
        this.play("error1");
    },

    playStarsAddToTotalSound: function () {
        this.play("star");
    },

    playGameWonSound: function () {
        this.play("victory");
    },

    playLoseSound: function () {
        this.play("lose");
    },

    playUnlockSound: function () {
        this.play("success4");
    },

    //End generic sounds
    //Start unique sounds 

    playDogBarkSound: function() {
        var soundName = "";
        var randomDogBarkSound = m.randomInt(0, 4);
        switch (randomDogBarkSound) {
        case 0:
            soundName = "dogbark1";
            break;
        case 1:
            soundName = "dogbark2";
            break;
        case 2:
            soundName = "dogbark3";
            break;
        case 3:
            soundName = "dogbark4";
            break;
        case 4:
            soundName = "dogbark5"; 
            break; 
        }
        this.play(soundName);
    },
    
    playSwingSound: function(looping) {
        this.play("swing1");     
    }, 
    playBounceSound: function(){
        this.play("boing1"); 
    },
    playBoneBounceSound: function() {
        this.play("boing1");  
    }, 
    
    playFoulThrowSound: function() {
        this.play("error1");  
    }, 
     playWhooshSound: function() {
        this.play("whoosh1");  
         this.play("boing1");  
    }, 
    playSuccessfulThrowSound: function() {
        this.play("success1");  
    }, 

    playStarTabInAndOutSound: function() {
        var soundName = "";
        var starTabSound = m.randomInt(0, 3);
        switch (starTabSound) {
        case 0:
            soundName = "whoosh1";
            break;
        case 1:
            soundName = "whoosh2";
            break;
        case 2:
            soundName = "whoosh3";
            break;
        case 3:
            soundName = "whoosh4";
            break;
        }
        this.play(soundName);
    }, 
    
    playCollectStarSound: function() {
        this.play("collect"); 
    }, 
    
    playObstacleSound: function() {
        var soundname = "";
        var boneInGroundSound = m.randomInt(0, 2);
        switch (boneInGroundSound) {
        case 0:
            soundName = "boing7";
            break;
        case 1:
            soundName = "boing8";
            break;
        }
        this.play(soundName); 
    }, 
    

    
    playBoing2: function() {
        this.play("boing2");   
    },
    
    playBoing3: function() {
        this.play("boing3"); 
    }, 
    playBoing4: function() {
        this.play("boing4"); 
    }, 
    
    playBoing5: function() {
        this.play("boing5");   
    },
    playBoing6: function() {
        this.play("boing6"); 
    }, 
    
    
    playBestThrowSound: function() {
        this.play("success2"); 
    }, 
    
    playBackgroundSound: function () {
        this.play("background", true);
    },

     playHitSound: function () {
        this.play("hit1");
    },
    
    stopBackgroundSound: function () {
        this.stop("background");     
    }, 
    
    playAllBackgroundSounds: function() {
        this.playBackgroundSound(); 
    }, 
    
    stopAllBackgroundSounds: function () {
        this.stopBackgroundSound(); 
    }, 
    
    muteOrUnmute: function () {
        this.isMuted = !this.isMuted; 
        if(this.isMuted)
            this.stopAllBackgroundSounds();
        else 
            this.playAllBackgroundSounds(); 
    }, 
    
    play: function (fileName, looping) {
        if(!this.isMuted)
        {
            
            if (looping == undefined)
                looping = false;

            if (!looping) {
                if (this.isDebug)
                    au.play(fileName);
                
                else 
                    this.playOnDevice("contentaudio", fileName); 
            } else {
                if (this.isDebug)
                {
                       au.play(fileName, 1, true);
                }
                else 
                    this.playOnDevice("contentaudio", fileName+"/loop"); 
            }
        }
    },

    stop: function (fileName) {
        if (this.isDebug)
            createjs.Sound.stop(); 
        else 
            this.playOnDevice("contentaudio", fileName+"/stop"); 
    }, 
    
    playOnDevice: function (method, arg) {
        var url = method + (arg ? "/" + arg : "");
        location.href = "http://" + url;
        if (window[method]) window[method](arg);
    },
});
var soundManager = new soundManager();