koko.class('HighScoresView', 'view', {
    HighScoresView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
        layout = [
			{ name:'HighscoresPanel', type:'container', x:vp.centerX, y:vp.top+30},
			
			{ name:'highscores_bg', type:'sprite', id:'HighscoresBack', parent:'HighscoresPanel', regX:390 },
			{ name:'highscores_title', type:'bmptext', parent:'HighscoresPanel', fitWidth:315, text:lang.getTerm('highscores'), size:'45px', align:'center', font:lang.getFontName(), tint:0xf24321, y: 14 },
			
			
			{ name:'scores_cont', type:'container', parent:'HighscoresPanel', x:-353, y:95 },
 
			{ name:'HighscoresHighlights', type:'container', parent:'scores_cont', x:0, y:0 },
			{ name:'HighlightBlue', type:'sprite', id:'HighlightBlue', parent:'HighscoresHighlights', x:-10, y:0 },
			{ name:'HighlightWhite', type:'sprite', id:'HighlightWhite', parent:'HighscoresHighlights', x:-10, y:0, visible:false },
		];
		
		for ( var i =0; i < 5; i++ ) {
			layout = layout.concat([
				{ name:'entry1'.replace('1', i+1), type:'container', parent:'scores_cont', y:88*i },
			    { name:'entry1_avatar'.replace('1', i+1), type:'container', parent:'entry1'.replace('1', i+1), x:100, y:20 },
				{ name:'entry1_rank_txt'.replace('1', i+1), type:'bmptext', parent:'entry1'.replace('1', i+1), x:50, y:20, text:(i+1).toString(), size:'45px', font:lang.getFontName(), tint:0xa6e1 },
				{ name:'entry1_distance_txt'.replace('1', i+1), type:'bmptext', parent:'entry1'.replace('1', i+1), fitWidth: 100, x:451, y:20, text:'256', size:'45px', font:lang.getFontName(), tint:0xa6e1 },
				{ name:'entry1_name_txt'.replace('1', i+1),  type:lang.textType(), font:lang.getFontName(true), parent:'entry1'.replace('1', i+1), x:190, y:20+10, text:'--', size:'45px', tint:0x0, visible:!user.unsupportedChars },
				{ name:'entry1_name_txtText'.replace('1', i+1),  type:'text', parent:'entry1'.replace('1', i+1), x:190, y:20+10, text:'--', size:'45px', tint:0x0, visible:user.unsupportedChars },
			
				{ name:'entry1_stars'.replace('1', i+1), type:'container',  parent:'entry1'.replace('1', i+1), x:570, y:9, s:0.89 },
					{ name:'entry1_stars_bg'.replace('1', i+1), type:'sprite', id:'StarsTotalBack', parent:'entry1_stars'.replace('1', i+1) },
					{ name:'entry1_stars_txt'.replace('1', i+1), type:'bmptext', parent:'entry1_stars'.replace('1', i+1), align:'center', text:'22', size:'32px', font:lang.getFontName(), tint:0xffffff, align:'center', x: 36, y: 22 },
			]);
		}

		this.children = d.buildLayout(layout, this.container);
        
		this.elements = [];
    },
	populateBoard: function(){
		var c = this.children;
		
		var scores = [];
		
		for( var i =0 ;i < user.users.length; i++ ){
			var u = user.users[i];
            if(u.id == user.GUEST) continue; 
			if(u.highScore[ game.difficulty ] == 0) continue;
			scores.push({
                id: u.id, 
				name: u.name,
				score: game.formatScore(u.highScore[ game.difficulty ]),
				stars: u.totalstars
			});
		}
		
		scores.sort(function(a, b) {
			return a.score > b.score ? -1 : 1;
		});
		
        /**
        var layout = [{ name:'NowPlayingAsImage', type:'bitmap', id:'av'+user.player1Id.toString(), x:-170, y:-33, scale:.48 }]
        var avatarImage = d.buildLayout(layout,  this.children.NowPlayingAs);
        ***/ 
        
        //scores.sort
		this.children.HighscoresHighlights.visible = false;
		for( var i =0; i < (scores.length > 5 ? 5 : scores.length); i++ ){
			c['entry1_distance_txt'.replace('1', i+1)].text = lang.getTerm('m').replace('p', scores[i].score.toString());
			c['entry1_name_txt'.replace('1', i+1)].text = lang.reverseString(scores[i].name);
            c['entry1_name_txtText'.replace('1', i+1)].text = lang.reverseString(scores[i].name);
			c['entry1_stars_txt'.replace('1', i+1)].text = scores[i].stars;
			
			if(user.name == scores[i].name) {
				this.children.HighscoresHighlights.visible = true;
				this.children.HighscoresHighlights.y = 88 * i;
				this.children.HighlightBlue.visible = i % 2 == 0;
				this.children.HighlightWhite.visible = i % 2 == 1;
			}

            var vp = app.viewPort;
            var texture = PIXI.Texture.fromImage(koko.assets.getAssetURL('av'+scores[i].id), true);
            var avatarScale = (138 / texture.width) * .48;
            
            var layout = [{ name:'AvatarImage', type:'bitmap', id:'av'+scores[i].id, x:-5, y:-8, scale:avatarScale }]; 
            d.buildLayout(layout,  c['entry1_avatar'.replace('1', i+1)])
		}
		
		for( var i = scores.length; i < 5; i++) {
			c['entry1_distance_txt'.replace('1', i+1)].text = '--';
			c['entry1_name_txt'.replace('1', i+1)].text = '--';
            c['entry1_name_txtText'.replace('1', i+1)].text = '--';
			c['entry1_stars_txt'.replace('1', i+1)].text = '-'
		}
	},
    transitionIn: function(exit){
		this.buildView();
		this.show();
		this.enable();
		
		this.populateBoard()
		
		v.get('menuhud').setState('highscores');
	
		d.applyThenAnimate(this.container, { alpha: 0 }, .3, { alpha: 1});
    },
    transitionOut: function(){
		this.destroyView();  
		this.hide();
		v.get(this.exitMenu).transitionIn(false);
    }
});