koko.class('SelectUserView', 'view', {
    SelectUserView: function () {
        this.superr.view.call(this);
    },
    buildView: function () {
        var g = app.guiSize,
            vp = app.viewPort;

        var layout = [
            {
                name: 'SelectUserContainer',
                type: 'container',
                id: 'SelectUserContainer',
                x: 0,
                y: 0
            },
            {
                name: 'shape_BlackOverlay',
                type: 'shape',
                parent: 'SelectUserContainer',
                x: 0,
                y: 0,
                fill: 0x000000,
                alpha: .7,
                w: 1136,
                h: 768,
                regX: 0,
                regY: 0
            },
            {
                name: 'SelectUserPanel',
                type: 'container',
                id: 'SelectUserPanel',
                parent: 'SelectUserContainer',
                x: vp.centerX,
                y: vp.centerY
            },
            {
                name: 'SelectUserBack',
                type: 'sprite',
                id: 'SelectUserBack',
                parent: 'SelectUserPanel',
                x: -308,
                y: -262
            },



            {
                name: 'Slider',
                type: 'container',
                x: 110,
                parent: 'SelectUserPanel'
            },
            {
                name: 'SelectUserPic',
                type: 'container',
                id: 'SelectUserPic',
                parent: 'SelectUserPanel',
                x: -79,
                y: -162
            },
            {
                name: 'SelectUserSurround',
                type: 'sprite',
                id: 'SelectUserSurround',
                parent: 'SelectUserPic',
                x: 0,
                y: 0
            },
            {
                name: 'WhiteLeftButton',
                type: 'sprite',
                id: 'WhiteLeftButton',
                parent: 'SelectUserPanel',
                x: -188,
                y: 50,
                regX: 27,
                regY: 27
            },
            {
                name: 'WhiteRightButton',
                type: 'sprite',
                id: 'WhiteRightButton',
                parent: 'SelectUserPanel',
                x: 195,
                y: 50,
                regX: 27,
                regY: 27
            },
    

            {
                name: 'SelectUserName',
                type: 'container',
                id: 'SelectUserName',
                parent: 'SelectUserPanel',
                x: -146,
                y: 18
            },
            {
                name: 'SelectUserNameBack',
                type: 'sprite',
                id: 'SelectUserNameBack',
                parent: 'SelectUserName',
                x: 0,
                y: 0
            },
            {
                name: 'SelectUserTick',
                type: 'sprite',
                id: 'SelectUserTick',
                parent: 'SelectUserPanel',
                x: 0,
                y: 176,
                regX: 59,
                regY: 59
            },
            { name:'SelectUserNameTextField', type:lang.textType(), font:lang.getFontName(true), parent:'SelectUserName', x:146, y:15, align:'center',text:'', size:'34px', lw:242, tint:0xf24321, visible:!user.unsupportedChars },
        { name:'SelectUserNameTextFieldText', type:'text', parent:'SelectUserName', x:146, y:15, align:'center',text:'', size:'200px', lw:242, tint:0xf24321, visible:user.unsupportedChars },
        
            {
                name: 'SelectUserPanelTitleTextField',
                type: 'bmptext',
                parent: 'SelectUserPanel',
                x: 0,
                y: -234,
                align: 'center',
                text: lang.getTerm('selectplayer'),
                size: '36px',
                font: lang.getFontName(),
                lw: 391,
                tint: 0xa6e1
            },

        ];


        layout = layout.concat(this.buildUserList());
        this.children = koko.display.buildLayout(layout, this.container)
        d.mask(this.children.Slider, -363, -175, 515, 200);

        var offset = 0
        var t = this
        this.slider = new DragSliderContainer(this.children.Slider, 'x', 110, 160, user.loadedAvatar.length - 1 - offset, {
            complete: function (drag) {

                var AVID = this.children.Slider.getChildAt(this.slider.index).getChildAt(0).avID
                this.children.SelectUserNameTextField.text = lang.reverseString(user.loadedAvatar[AVID].avatar_name);
                this.children.SelectUserNameTextFieldText.text = lang.reverseString(user.loadedAvatar[AVID].avatar_name);
            }.bind(this)
        });

        this.slider.gotoSlide(user.findAvatarByID(user.id));

        var select_btn = new Button(this.children.SelectUserTick, {
            scale: 1.1
        }, {
            click: function () {
                soundManager.playButtonSound();
                var AVID = t.children.Slider.getChildAt(t.slider.index).getChildAt(0).avID1;
                t.transitionOut();
                user.selectUser(AVID)
                v.get('menuhud').setState('mainmenu');
                v.get('userloggedin').transitionIn();
            }
        });

        var next_btn = new Button(this.children.WhiteRightButton, {
            scale: 1.1
        }, {
            click: function () {
                soundManager.playButtonSound();
                this.slider.gotoNextSlide(.35);
            }.bind(this)
        });

        var previous_btn = new Button(this.children.WhiteLeftButton, {
            scale: 1.1
        }, {
            click: function () {
                soundManager.playButtonSound();
                this.slider.gotoPreviousSlide(.35);
            }.bind(this)
        });

        this.elements = [this.slider, select_btn, next_btn, previous_btn];
    },
    buildUserList: function () {
        var carousel = [];
        var offset = 0;
        for (var i = 0; i < user.loadedAvatar.length; i++) {
            var avID = 'av' + i
            var avName = 'av' + user.loadedAvatar[i].avatar_id;
            
            var texture = PIXI.Texture.fromImage(koko.assets.getAssetURL(avName), true);
            var avatarScale = 138 / texture.width;
    
            
            var charX = 0 + ((i - offset) * 160);
            var userArray = [
                {
                    name: avID,
                    type: 'container',
                    parent: 'Slider',
                    x: charX,
                    y: 81,
                    regX: 175,
                    regY: 230,
                    },
                {
                    name: 'AvImage',
                    type: 'bitmap',
                    id: avName,
                    parent: avID,
                    scale:avatarScale,
                    avID: i,
                    avID1: user.loadedAvatar[i].avatar_id
                }
            ]
            carousel = carousel.concat(userArray);
        }
        return carousel;
    },

    transitionIn: function () {
        this.container.alpha = 1;
        var vp = app.viewPort;

        this.buildView();
        this.show();

        v.all(function (v) {
            if (v.container.visible) v.disable();
        });

        d.applyThenAnimate(this.children.SelectUserPanel, {
            y: vp.centerY + 20,
            alpha: 0
        }, .3, {
            delay: 0.1,
            alpha: 1,
            y: vp.centerY
        });

        this.enable();
    },
    transitionOut: function () {
        this.disable();
        d.animateObject(this.container, .2, {
            alpha: 0,
            onComplete: function () {
                v.all(function (v) {
                    if (v.container.visible) v.enable();
                });

                this.destroyView();
                this.hide();

            }.bind(this)
        });
    }
});