<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.3.4</string>
        <key>fileName</key>
        <string>//KOKO-NAS/Qweb/kinderfingerserve/_textures/buttons.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename>../buttons.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../buttons.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>buttons/PauseButton.png</filename>
            <filename>buttons/PlayAgainButton.png</filename>
            <filename>buttons/PlayButton.png</filename>
            <filename>buttons/PreviousButton.png</filename>
            <filename>buttons/RestartButton.png</filename>
            <filename>buttons/ResumeButton.png</filename>
            <filename>buttons/SoundOff.png</filename>
            <filename>buttons/SoundOn.png</filename>
            <filename>buttons/StoryNextButton.png</filename>
            <filename>buttons/SwitchUserButton.png</filename>
            <filename>buttons/WindArrowGood.png</filename>
            <filename>buttons/YesNoButtonBack.png</filename>
            <filename>buttons/CloseButton.png</filename>
            <filename>buttons/HighscoresButton.png</filename>
            <filename>buttons/MainMenuLargeButton.png</filename>
            <filename>buttons/MainMenuSmallButton.png</filename>
            <filename>buttons/NextButton.png</filename>
            <filename>buttons/WindArrowGood.png</filename>
            <filename>buttons/YesNoButtonBack.png</filename>
            <filename>buttons/CloseButton.png</filename>
            <filename>buttons/HighscoresButton.png</filename>
            <filename>buttons/MainMenuLargeButton.png</filename>
            <filename>buttons/MainMenuSmallButton.png</filename>
            <filename>buttons/NextButton.png</filename>
            <filename>buttons/PauseButton.png</filename>
            <filename>buttons/PlayAgainButton.png</filename>
            <filename>buttons/PlayButton.png</filename>
            <filename>buttons/PreviousButton.png</filename>
            <filename>buttons/RestartButton.png</filename>
            <filename>buttons/ResumeButton.png</filename>
            <filename>buttons/SoundOff.png</filename>
            <filename>buttons/SoundOn.png</filename>
            <filename>buttons/StoryNextButton.png</filename>
            <filename>buttons/SwitchUserButton.png</filename>
            <filename>buttons/WindArrowBad.png</filename>
            <filename>buttons/RestartButton.png</filename>
            <filename>buttons/ResumeButton.png</filename>
            <filename>buttons/SoundOff.png</filename>
            <filename>buttons/SoundOn.png</filename>
            <filename>buttons/StoryNextButton.png</filename>
            <filename>buttons/SwitchUserButton.png</filename>
            <filename>buttons/WindArrowBad.png</filename>
            <filename>buttons/WindArrowGood.png</filename>
            <filename>buttons/YesNoButtonBack.png</filename>
            <filename>buttons/CloseButton.png</filename>
            <filename>buttons/HighscoresButton.png</filename>
            <filename>buttons/HowToPlaySkipButton.png</filename>
            <filename>buttons/MainMenuLargeButton.png</filename>
            <filename>buttons/MainMenuSmallButton.png</filename>
            <filename>buttons/NextButton.png</filename>
            <filename>buttons/PauseButton.png</filename>
            <filename>buttons/PlayAgainButton.png</filename>
            <filename>buttons/PlayButton.png</filename>
            <filename>buttons/PreviousButton.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
