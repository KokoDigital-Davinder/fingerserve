﻿package
{
	
	
	import com.adobe.images.PNGEncoder;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.utils.describeType;
	
	public class LayoutJSGenerator extends MovieClip
	{
		
		
		private var layoutNodes:Array = new Array();
		
		private var codeOutput:String = new String();
		private var _exportImages:Boolean = true;
		
		public function LayoutJSGenerator()
		{
			// constructor code
			generateJavascript();
		}
		
		private function generateJavascript():void
		{
			//trace("SUCCESS");
			//trace("PP:", Bitmap(getChildAt(0).getChildAt(0)).x);
			//trace("PP:", getChildAt(0).getChildAt(0).y);
			
			
			codeOutput = '[\r';
			
			loopThroughMovieClip(this);
			
			codeOutput = codeOutput.slice(0, -2);
			codeOutput += '\r]';
			trace(codeOutput);
			
			savePNG(MovieClip(getChildAt(0)));
			
		}
		
		private function loopThroughMovieClip($mc:MovieClip):void
		{
			for (var i:uint = 0; i < $mc.numChildren; i++)
			{
				
				var targetMC:MovieClip = $mc.getChildAt(i) as MovieClip;
				
				if (targetMC)
				{
					
					if (targetMC.numChildren == 1)
					{
						//Asset only contains one child so it must be a graphic that needs saving.
						if (targetMC.name.substr(0, 6) == 'shape_')
						{
							codeOutput += generateNodeString('shape', targetMC);
						}
						else if (targetMC.name.substr(0, 5) == 'mask_')
						{
							codeOutput += generateNodeString('mask', targetMC);
						}
						else
						{
							if (targetMC.getChildAt(0) is TextField)
							{
								codeOutput += generateNodeString('bmptext', targetMC);
							}
							else
							{
								codeOutput += generateNodeString('sprite', targetMC);
								savePNG(targetMC);
							}
						}
						codeOutput += ',\r';
					}
					else
					{
						//Asset contains more than one child so it needs to be looped through
						codeOutput += generateNodeString('container', targetMC)
						codeOutput += ',\r';
						loopThroughMovieClip(targetMC);
						
					}	
				}
				else
				{
					//trace("SHAPE: "+targetMC.name)
				}
			}
			
		
			
		}
		
		private function generateNode($type:String, $containerMC:MovieClip):void 
		{
			var node:Object = new Object();
			
			node.x = Math.round($containerMC.x);
			node.y = Math.round($containerMC.y);

			if ($containerMC.parent != this) node.parent = $containerMC.parent.name;
			if (!$containerMC.visible)
			{
				node.visible = false;
				$containerMC.visible = true;
			}
	
			
			if ($containerMC.scaleX != 1 || $containerMC.scaleY != 1)
			{
				if ($containerMC.scaleX == $containerMC.scaleY) node.s = int($containerMC.scaleX * 100) / 100;
				else
				{
					if ($containerMC.scaleX != 1) node.sX = int($containerMC.scaleX * 100) / 100;
					if ($containerMC.scaleY != 1) node.sY = int($containerMC.scaleY * 100) / 100;
				}
				$containerMC.scaleX = 1;
				$containerMC.scaleY = 1;
			}
			
			
			if ($containerMC.rotation != 0) 
			{
				//Set node and rest back to 0 ready for image output
				node.rotation = int($containerMC.rotation * 10) / 10;
				$containerMC.rotation = 0;
			}
			if ($containerMC.alpha != 1) 
			{
				//Set node and reset alpha back to 1 ready for image output
				node.alpha = $containerMC.alpha;
				$containerMC.alpha = 1;
			}
			
			var boundRect:Rectangle = $containerMC.getBounds($containerMC);
			if (boundRect.x != 0) node.regX = Math.round(boundRect.x * -1);
			if (boundRect.y != 0) node.regY = Math.round(boundRect.y * -1);
			
			node.name = $containerMC.name;
			node.id = $containerMC.name;
			node.type = $type;
			
			
			trace(objectToString(node, ','));
			
			
			layoutNodes.push(node);
		}
		
		private function generateNodeString($type:String, $containerMC:MovieClip):String 
		{
			var node:String = new String();
			node='{ '
			node += 'name:\'' + $containerMC.name + '\', ';
			node += 'type:\'' + $type + '\', ';
			if ($type != 'shape' && $type != 'mask' && $type != 'bmptext' ) node += 'id:\'' + $containerMC.name + '\', ';
			if ($containerMC.parent != this) node += 'parent:\'' + $containerMC.parent.name + '\', ';
			node += 'x:' + Math.round($containerMC.x) + ', ';
			node += 'y:' + Math.round($containerMC.y) + ', ';

			
			if ($type == 'shape')
			{
				node+= 'fill:0x' + getShapeColour($containerMC) + ', ';
				node+= 'w:' + $containerMC.width + ', ';
				node+= 'h:' + $containerMC.height + ', ';
			}
			
			if ($type == 'mask')
			{
				node+= 'w:' + $containerMC.width + ', ';
				node+= 'h:' + $containerMC.height + ', ';
			}
			
		
			if ($type == 'bmptext')
			{
				var textField:TextField = $containerMC.getChildAt(0) as TextField;
				node+= 'text:\'' + textField.text + '\', ';
				node+= 'size:\''+ textField.getTextFormat().size +'px\', '
				node+= 'font:\'' + textField.getTextFormat().font + '\', ';
				node+= 'lw:' + textField.width + ', ';
				var colour:Object = textField.getTextFormat().color;
				node+= 'tint:0x' +uint(colour).toString(16) + ', ';
			}
			
			
			if (!$containerMC.visible)
			{
				node += 'visible:false, ';
				$containerMC.visible = true;
			}
	
			
			if ($containerMC.scaleX != 1 || $containerMC.scaleY != 1)
			{
				if ($containerMC.scaleX == $containerMC.scaleY) node += 's:' + int($containerMC.scaleX * 100) / 100 + ', ';
				else
				{
					if ($containerMC.scaleX != 1) node += 'sX:' + int($containerMC.scaleX * 100) / 100 + ', ';
					if ($containerMC.scaleY != 1) node += 'sY:' + int($containerMC.scaleY * 100) / 100 + ', ';
				}
				$containerMC.scaleX = 1;
				$containerMC.scaleY = 1;
			}
			
			
			if ($containerMC.rotation != 0) 
			{
				//Set node and rest back to 0 ready for image output
				node += 'rotation:' + int($containerMC.rotation * 10) / 10 + ', ';
				$containerMC.rotation = 0;
			}
			if ($containerMC.alpha != 1) 
			{
				//Set node and reset alpha back to 1 ready for image output
				node += 'alpha:' + $containerMC.alpha + ', ';
				$containerMC.alpha = 1;
			}
			
			if ($type != 'container')
			{
				var boundRect:Rectangle = $containerMC.getBounds($containerMC);
				if (boundRect.x != 0) node += 'regX:' + Math.round(boundRect.x * -1) + ', ';
				if (boundRect.y != 0) node += 'regY:' + Math.round(boundRect.y * -1) + ', ';
			}
			
			node = node.slice(0, -2);
			node+=' }'
			
			
		//	trace(node);
			
			return node;
			//layoutNodes.push(node);
		}
		
		
		
		public  function objectToString(obj:Object=null, delimiter:String = "\n"): String
		{
			if (obj == null || delimiter == null)
			{
				return "";
			}
			else
			{
				var ret:Array = [];
				for (var s:Object in obj)
				{
					ret.push(s + ":" + obj[s]);
				}
				return ret.join(delimiter);
			}
		}
		
		private function getShapeColour($mc:MovieClip):String
		{

				var bounds:Rectangle = $mc.getBounds($mc);
				var bmp_data:BitmapData = new BitmapData($mc.width, $mc.height);
				bmp_data.draw($mc, new Matrix( 1, 0, 0, 1, - bounds.x, - bounds.y ));
				//trace(bmp_data.getPixel(0, 0).toString(16));
				return bmp_data.getPixel(0, 0).toString(16);
		}
		
		
		private function savePNG($mc:MovieClip)
		{
			if(!_exportImages) return; 
			
			var bmp_data:BitmapData = new BitmapData($mc.width, $mc.height, true, 0x0000000);
			var bounds:Rectangle = $mc.getBounds($mc);
			bmp_data.draw($mc, new Matrix( 1, 0, 0, 1, - bounds.x, - bounds.y ));
			var png:ByteArray = PNGEncoder.encode(bmp_data);
			
			var filename:String = $mc.name+".png";
			var fileReference:File = File.applicationDirectory.resolvePath( filename );
			var wr:File = new File(fileReference.nativePath);
			
			var stream:FileStream = new FileStream();
			stream.open(wr, FileMode.WRITE);
			stream.writeBytes(png, 0, png.length);
			stream.close();
		}
	
	}
}
