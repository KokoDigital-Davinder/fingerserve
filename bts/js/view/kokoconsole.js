koko.class('KokoConsoleView', 'view', {
    
    KokoConsoleView: function()
    {
        this.superr.view.call(this);
        if(kokoconsole) kokoconsole.isInitiated = true;
    },
    
    logged:[],
    
    minimised:false,
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort, t = this;
        this.children = koko.display.buildLayout([
            { name:'logContainer',  type:'container', x:vp.left, y:vp.top},
			{ name:'bg', type:'shape', alpha:0.9, parent:'logContainer', fill:0xffffff, width:app.viewPort.width, height:350},
			{ name:'logText', type:'text', y:30, parent:'logContainer', x:20, size:'10px', scale:.5},
            { name:'fpsBar', type:'shape', alpha:1, fill:0xffffff, width:app.viewPort.width, height:30, x:vp.left, y:vp.top },
            { name:'fpsLine', type:'shape', alpha:1, fill:0x000000, width:app.viewPort.width, height:1, x:vp.left, y:vp.top+30 },
            { name:'fpsText', type:'text', y:10, tint:0xff0000, x:10, text:'FPS: 60', scale:.5},
		], this.container);
		
       
       this.children.fpsBar.hitArea = new PIXI.Rectangle(0, 0, app.viewPort.width, 30);
       
		var closeBar = new Button(this.children.fpsBar, {}, {
			click: function(){
                if(t.minimised)
                {
                    t.minimised = false;
                    t.children.logContainer.visible = true;
                }
                else
                {
                    t.minimised = true;
                    t.children.logContainer.visible = false;
                }
			}.bind(this)
		});

		
        this.elements = [closeBar ];
    },
    addLogText: function($txt)
    {
        this.logged.push($txt);
        if(this.logged.length > 50) this.logged.shift();
        this.children.logText.text  = '';
        for(var i=0; i<this.logged.length; i++)
        {
             this.children.logText.text += '\n'+this.logged[i];
        }
        if(this.children.logText.height > 300)
        {
            var diff = 300 - this.children.logText.height
            this.children.logText.y =  40 +diff;
        }
    },
    counter:0,
	getFPS : function(){
        this.counter++
        if(this.counter > 5)
        {
            this.counter = 0;
            this.children.fpsText.text = 'FPS: '+Math.ceil(PIXI.ticker.shared.FPS);
        }
            
    },
    
    resize: function() {
        var c = this.children, g = app.guiSize, vp = app.viewPort;
        
        //c.bg.x = vp.left;
        //c.bg.y = vp.top;  
        
        c.fpsBar.x = vp.left;
        c.fpsBar.y = vp.top; 
        c.fpsLine.x = vp.left;
        c.fpsLine.y = vp.top + 30;
        
        c.logContainer.x = vp.left;
        c.logContainer.x = vp.left;
        
        c.fpsText.x = vp.left + 20;
        c.fpsText.y = vp.top + 8;
		
    },
    transitionIn: function()
    {
        var t = this;
        app.addUpdateListener('kokoconsole', function(){ t.getFPS()});
        console.log("TIN")
		this.buildView();
		this.show();
		this.enable();
           app.addResizeListener('kokoconsole', this.resize.bind(this));
    },
    transitionOut: function()
    {   
		this.destroyView();  
		this.hide();
    } 
});

koko.class('KokoConsole', '', {

    isInitiated:false,
    KokoConsole: function()
    {
        v.add('kokoconsoleview',      new KokoConsoleView(),                 app.stage);
        v.get('kokoconsoleview').transitionIn();
    },
    log: function($logTxt)
    {
        v.get('kokoconsoleview').addLogText($logTxt);
    }


});
var kokoconsole;
function initiaiteKokoConsole(){ kokoconsole = new KokoConsole(); }
