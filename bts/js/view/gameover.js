koko.class('GameOverView', 'view', {
    GameOverView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
        
        this.children = koko.display.buildLayout([
			
			{ name:'panel_cont', type:'container',  x:vp.centerX, y:100 },
			{ name:'panel_bg', type:'sprite', id:'GOBack', parent:'panel_cont', x:0, y:0, regX:304 },

			//info
			{ name:'winner_txt', type:'bmptext', parent:'panel_cont', fitWidth:315, y:15, text:lang.getTerm('welldone'), size:'45px', font:lang.getFontName(), tint:0xf24321, align:'center' },
			{ name:'title_txt', type:'bmptext', parent:'panel_cont', fitWidth:262,  text:lang.getTerm('yourbestthrow'), size:'31ppx', font:lang.getFontName(),  tint:0x999999, align:'center', y:90 },
			{ name:'score_txt', type:'bmptext', parent:'panel_cont', text:'584', size:'100px', font:lang.getFontName(), tint:0xa6e1, align:'center', y:115 },
			
			//personal best
			{ name:'pb_banner', type:'container', parent:'panel_cont', y:220, visible:false },
				{ name:'pb_back', type:'sprite', id:'PersonalBestBack', parent:'pb_banner', regX:200 },
				{ name:'pb_title_txt', type:'bmptext', parent:'pb_banner', text:lang.getTerm('yourpb'), size:'24px', font:lang.getFontName(), tint:0xf7b72f, x:-150, y:14 },
				{ name:'pb_distance_txt', type:'bmptext', parent:'pb_banner', text:'516', size:'24px', font:lang.getFontName(),  tint:0xf28b23, y: 14, x: 150, align:'right' },
			
			{ name:'new_pb', type:'container', parent:'panel_cont' },
			{ name:'new_pb_banner', type:'sprite', id:lang.getTerm('new_best_sprite_id'), y:250, regX:190, regY:30, parent:'new_pb' },
			{ name:'new_pb_stars', type:'sprite', id:'GameOverBestStars', parent:'new_pb', y:100, regX:213 },
			
			//stars collects
			{ name:'stars_collected_bg', type:'sprite', id:'GameOverStarsCollectedIcon', parent:'panel_cont', y: 300, x: -70 },
			{ name:'stars_collected_txt', type:'bmptext', parent:'panel_cont', y:308, x: 30, text:'3', size:'60px', font:lang.getFontName(), tint:0xa6e1, align:'left' },
			
			//replay
			{ name:'play_btn', type:'sprite', id:'PlayAgainButton', x:-70, y:470, regX:65, regY:65, parent:'panel_cont' },
			{ name:'home_btn', type:'sprite', id:'MainMenuSmallButton', x:70, y:470, regX:65, regY:65, parent:'panel_cont' },
			
		], this.container);
		
		var play_btn = new Button(this.children.play_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound(); 
				this.transitionOut();
				this.exitThisMenu('ingame');
			}.bind(this)
		});
		
		var home_btn = new Button(this.children.home_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				this.transitionOut();
				this.exitThisMenu('mainmenu');
			}.bind(this)
		});
		
        this.elements = [ play_btn, home_btn ];
    },
	exitThisMenu: function(to) {
        v.get(to).transitionIn();
	},
	resize: function(){
		var c = this.children, g = app.guiSize, vp = app.viewPort;
		
	},
    transitionIn: function(){
        this.buildView();
        this.show();
        this.enable();
		
		v.get('menuhud').setState('mainmenu');
		
		var best = user.attempts[0];
		for( var i =1; i< user.attempts.length; i++){
			if(user.attempts[i][0] > best[0]) best = user.attempts[i];
		}
		
		if( best[1] == -1 ) best[1] = "0";
              if(best[1] == 0) this.children.winner_txt.text = lang.getTerm('toobad')
		
		this.children.score_txt.text = lang.getTerm('m').replace('p', best[1]);
		this.children.stars_collected_txt.text = user.stars;
		
		d.apply(this.children.new_pb_banner, { visible:false });
		d.apply(this.children.new_pb_stars, { visible:false });
		d.apply(this.children.pb_banner, { visible:false });
		
		
		if(user.score[game.difficulty] == user.highScore[game.difficulty]) {
			//high scores
            soundManager.playBestThrowSound()
			d.applyThenAnimate(this.children.new_pb_banner, { visible:true, scale:0 }, .3, { delay:0.5, scale:1, ease:Back.easeOut });
			d.applyThenAnimate(this.children.new_pb_stars, { visible:true, alpha:0, }, 1, { delay:0.5, alpha:1 });
		} else {
			d.apply(this.children.pb_banner, { visible:true } );
			this.children.pb_distance_txt.text = lang.getTerm('m').replace('p', game.formatScore(user.highScore[game.difficulty]))
		}
		
		
		d.applyThenAnimate(this.container, { alpha:0 }, .2, { alpha:1 });
		
		app.addResizeListener('gameover', this.resize.bind(this));
    },
    transitionOut: function(){
		
		d.animateObject(this.container, .3, { alpha:0, onComplete: function(){
			this.destroyView();  
			this.hide();
			app.removeResizeListener('gameover');
		}.bind(this)});
    }
});