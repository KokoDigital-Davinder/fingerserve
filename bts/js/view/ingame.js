koko.class('InGameView', 'view', {
    assets: [{ id:'gg_json', src:'img/gamegraphic.json'}],
    InGameView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var t = this, g = app.guiSize, vp = app.viewPort;
        
        this.children = koko.display.buildLayout([
			{name:'environment', x: GAME_CONST.startX, y: 260 },
			{ name:'ball', type:'sprite', id:'ball', parent:'environment', x: 100, y: 400, regX:40, regY:40 },
			
			{ name:'hit_btn', type:'shape', width:1, height:1 },
			{ name:'line', type:'shape', width:g.width, height:2, x:0, y:260+GAME_CONST.floorY}
        ], this.container);
		
		this.children.hit_btn.hitArea = new PIXI.Rectangle(0, 0, g.width, g.height);
		
		this.bat = new FingerView();
		this.bat.transitionIn();
		this.children.environment.addChildAt(this.bat.container, 0);
		
		var hit_btn = new Button(this.children.hit_btn, null, {
			down: function(){
				game.throw();
			}.bind(this)
		});
		
		this.children.hit_btn.buttonMode = false; //disables the mousepointer being enabled over the entire screen
		
        this.elements = [ hit_btn ];
    },
	keepup: function(){
		if(!this.bat.isPlaying) 
        {
            this.bat.playAnim('keepup');
            soundManager.playBounceSound();
        }
       
		this.children.environment.setChildIndex(this.bat.container, 0);
	},
	hit: function(){
		if(this.bat.currentAnim == 'hit') return;
        soundManager.playBoing4();
        soundManager.playWhooshSound();
		this.bat.playAnim('hit');	
		this.children.environment.setChildIndex(this.bat.container, this.children.environment.children.length-1);
	},
    transitionIn: function(){
        this.buildView();
        this.show();
        this.enable();
		
		this.children.line.visible = game.shouldSetDefaultObstacles;
		
		v.get('menuhud').setState('ingame');
		
		d.applyThenAnimate(this.container, { alpha:0 }, .3, { alpha:1 });
		
        v.get('ingamehud').transitionIn();
    },
    transitionOut: function(){
        this.stopGame();
        this.destroyView();  
        this.hide();
        v.get('ingamehud').transitionOut();
    },
    disable: function() {
        this.superr.disable.call(this);
        v.get('ingamehud').disable();
    },
    enable: function() {
        this.superr.enable.call(this);
        v.get('ingamehud').enable();
    },
    startGame: function() {
        this.enable();
        
		//game start
       	game.start();
		
        //add update
		app.addUpdateListener('ingame', function(delta){ 
            game.update(delta);
            this.update(delta); 
        }.bind(this));
    },
    stopGame: function(){
        game.stop();
        app.removeUpdateListener('ingame');
        game.save();
    },
    setupThrow: function(){
        var c = this.children, ball = c.ball;

       	d.applyProps(ball, {x:100, y: GAME_CONST.floorY-100, rotation:6 });
    },
    addObstacle: function(obs){ 
        var img = obs.movie;
        
        img.x = obs.pos.x + obs.img_offset.x;
        img.y = obs.pos.y + obs.img_offset.y;
        
        this.children.environment.addChildAt(img, 0);
    },
	releaseBall: function(){
        var c = this.children, ball = c.ball;
		
		var rotation = 45;
		var point = m.pointFromDistanceAngle({x:ball.x, y: GAME_CONST.floorY}, rotation+90, 82);

		d.applyProps(ball, { y: point.y-100, rotation: ball.rotation-90, visible: true}, true);
		
		game.setPosition(point.x, point.y-100, m.radiansToAngle(ball.rotation));
    },
    update: function(){
        var environment = this.children.environment, ball = this.children.ball;
        
		environment.position.x = (game.xpos * -1) + GAME_CONST.startX;
		
		switch(game.state)
		{
			case GAME_STATE.SWINGING:
				ball.position.y = game.keepyup.currentY;
				ball.rotation += game.keepyup.rotationSpeed;
			break;
			case GAME_STATE.FINISHING:
			case GAME_STATE.WATCHING:
				if(!game.hasHit) {
					ball.position.y = game.keepyup.currentY;
					ball.rotation += game.keepyup.rotationSpeed;
				} else {
					ball.position.x = game.position.x < 100 ? 100 : game.position.x;
					ball.position.y = game.position.y;
					ball.rotation = game.position.rotation;
				}
			break;
		}
    }
});