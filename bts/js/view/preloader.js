koko.class('PreloaderView', 'view', {
    PreloaderView: function(){
        this.superr.view.call(this);
    },
    loadedPrelodaerGraphics: false,
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
        if(!this.loadedPrelodaerGraphics) {
            this.children = koko.display.buildLayout([
                { name:'bg', type:'shape', alpha:1, fill:0x808080, width:g.width, height:g.height }
            ], this.container);
        }      
    },
    setupProperGraphics: function(){
        var g = app.guiSize, vp = app.viewPort;
        var frameArray = [];
                this.children = koko.display.buildLayout([
                { name:'bg', type:'sprite',id:'fadedbackground' }
               
            ], this.container);
        for (var i = 1; i <= 11; i++) 
        {
            var frameID = i<10 ?'0'+i.toString() : i.toString() ;
            var spriteName = 'preloader00' + frameID + '.png';
            frameArray.push(new PIXI.Texture.fromFrame(spriteName));
        }
        
        var preloaderMC = new PIXI.extras.MovieClip(frameArray)
        preloaderMC.x = vp.centerX - 100
        preloaderMC.y = vp.centerY - 100
        preloaderMC.animationSpeed = .75;
        preloaderMC.play();
        this.container.addChild(preloaderMC)
        
        this.loadedPrelodaerGraphics = true;
    },
    transitionIn: function(){
		this.buildView();
		this.show();
    },
    gfxPerc: 0,
    sfxPerc: 0,
    updateSFXPerc: function(r){
        this.sfxPerc = r;
        this.updatePerc();
    },
    updateGFXPerc: function(r){
        this.gfxPerc = r;
        this.updatePerc();
    },
    updatePerc: function(){
        var g = app.guiSize, vp = app.viewPort;
        var perc = (this.gfxPerc + this.sfxPerc) * 0.5
        
        if(perc >= 1)
        {
            this.transitionOut();
        }
        
    },
    transitionOut: function(){
        this.disable();
		this.destroyView();  
		this.hide();
        main.preloadComplete();
    }
});