// GAME
var GAME_STATE = {
	WAITING: 0,
	SWINGING: 1,
	WATCHING: 2,
	WALKING: 3,
	FINISHING: 4,
}
var DIFFICULTY_SETTINGS = [
    {
        rotation: { max:9, acceleration: 0.2 },
        power: { max:55, acceleration:0.7 }
    },
    {
        rotation: { max:10, acceleration: 0.22 },
        power: { max:60, acceleration:0.8 }
    },
    {
        rotation: { max:11, acceleration: 0.24 },
        power: { max:70, acceleration:0.9 }
    }
];

var GAME_CONST = {
    foulThrowX:-150,
    distanceDragWidth: 200,
    floorY: 250,
    gravity: 1,//1.3,//0.6,
    maxThrows: 3,
	startX: 500,
	cameraFarX: 20
}

koko.class('gameController', null, {
	state: GAME_STATE.WAITING,
	gameController: function() { },
    difficulty:1,
	power: {
		max: 55,
		min: 6,
		acceleration: 0.7,//0.35
		current: 6	
	},
	wind: {
		current:0,
		target: 0,
		speed: 0,	
	},
	speed: {
		x:0,
		y:0,
		rotation:0
	},
	position: {
		x:0,
		y:0,
		rotation:0
	},
	keepyup: {
		gravity: 1.3,
		speed:0,
		minHitSpeed: 20, 
		maxHitSpeed: 40,
		lastHitSpeed: 0,
		hitY: GAME_CONST.floorY,
		minJumpY: 200,
		maxjumpY: -100,
		currentY:400,
		rotationSpeed:0,
	},
    paused: false,
	launchAngle: 0,
	stars: 0,
	distance:0,
    xpos: 0,
	attempts: [[0,0], [0,0], [0,0]],
	currentThrow:0,
	allowThrow: true,
    //const
	obstacle_pool: [],
    dontBounce: false,
	shouldSetDefaultObstacles:false,
	setupDefaultObstacles: function(){
		this.shouldSetDefaultObstacles = true;	
	},
	collectStar: function(){
		this.stars++;
	},
	reset: function() {
		for( var i = 0; i < this.attempts.length; i++ ) {
			this.attempts[i][0] = 0;
			this.attempts[i][1] = 0;
		}
		this.currentThrow = 0;
        this.distance = 0;
        this.launchAngle = 0;
        this.stars =0;
		this.allowThrow = false;		
	},
    setDifficulty: function(level) {
        var s = DIFFICULTY_SETTINGS[level];
        this.difficulty = level;

        this.power.max = s.power.max;
        this.power.acceleration = s.power.acceleration;
        this.rotation.max = s.rotation.max;
        this.rotation.acceleration = s.rotation.acceleration;
    },
	setPosition: function(x, y, r) {
		this.position.x = x;
		this.position.y = y;
		this.position.rotation = r;
	},
    save: function(){
        user.saveAttempts(this.attempts, this.stars, this.difficulty);
        this.reset();
    },
	changeState: function (s){
		//set state
		this.state = s;
		
		//do sumit then
		switch(s)
		{
			case GAME_STATE.WALKING:
                
            break;
			case GAME_STATE.SWINGING:
				this.allowThrow = true;
				this.power.current = 0;
				this.launchAngle = 0;
				
				this.wind.current = 0;
				this.wind.target = 0;
				this.wind.speed = 0;
				
				this.xpos = 0;
				this.distance = 0;
                this.position.x = GAME_CONST.foulThrowX;
				
				if(this.shouldSetDefaultObstacles){
					setTimeout(function(){
						game.throw();
					}, 100);
				}
			break;
			case GAME_STATE.WATCHING:
				this.allowThrow = false;
				this.xpos = 0;
				this.distance = 0;
				this.lastShownStar = 3000;
				
				//release the suticase
				v.get('ingame').releaseBall();
				
				//min power
				if(this.power.current < this.power.min) {
					this.power.current = this.power.min;
				}
				
				
				this.launchAngle = -45;
				this.power.current = this.keepyup.lastHitSpeed * 1.5;
				
				
				
                var windBoostedPower = this.power.current + (this.power.current * this.wind.current * 0.02);
				var s = m.pointFromDistanceAngle({x:0, y:0}, this.launchAngle, windBoostedPower);

				this.speed.x = s.x;
				this.speed.y = s.y;
				this.speed.rotation = Math.min(0.5,((this.speed.x*0.05)+((this.speed.x*0.05)*Math.random()))*0.2);

				//used for showcasing all obstacles to see their positions / hit areas
				if(this.shouldSetDefaultObstacles) {
					var startingX = 900;
					for( var i = 0; i < 8; i++ ){
						var obs;
						switch(i) {
							case 0:
								obs = new KinderinoBoost();
							break;
							case 1:
								obs = new ManholeBoost();
							break;
							case 2:
								obs = new MoleBoost();
							break;
							case 3:
								obs = new ParasolBoost();
							break;
							case 4:
								obs = new NetBoost();
							break;
							case 5:
								obs = new GymBagObstacle();
							break;
							case 6:
								obs = new ChairObstacle();
							break;
							case 7:
								obs = new BinObstacle();
							break;
            			}
						
						obs.setPosition(startingX);
						
						startingX += obs.pos.width+300;

						v.get('ingame').addObstacle(obs);
						obs.drawDebug();

						this.obstacle_pool.push(obs);
					}
					
				}
			break;
		}
	},
    formatScore: function(pxDist){
		pxDist += GAME_CONST.foulThrowX;
		if(pxDist < 0) pxDist = 0;
        return Math.ceil(pxDist*0.01)
    },
	start: function() {
        this.paused = false;
		this.prepareThrow();
		v.get('ingamehud').updateHeight(0);
	},
	prepareThrow: function() {
		v.get('ingame').setupThrow();
        this.startSwinging();
	},
	setupNextThrow: function() {
        if( this.xpos == 0 ){
			this.prepareThrow();
		} 
        else {
            var inter = Math.ceil(this.xpos*0.001);
            TweenLite.to(this, .2+(.2*inter), { xpos: 0, ease:Sine.easeOut, onComplete:function(){
                game.prepareThrow();
            }});
        }
	},
	startSwinging: function() {
		v.get('background').clearBackgroundItems();
        this.changeState(GAME_STATE.SWINGING);
        this.destroyObstacles();
	},
	throw: function() {
       
		//only if throw allowed y'no
		if(!this.allowThrow) return;
		//dunna bo
		this.dontBounce = false;
		this.changeState(GAME_STATE.WATCHING);

        
	},
	endThrow: function() {
        //console.log('end throw');
		var _t = this;
		//finishin
		//work out the progression,
		var thisAttempt = this.attempts[ this.currentThrow ];
		
		
		if(this.position.x < GAME_CONST.foulThrowX)
		{
            soundManager.playFoulThrowSound();
			//endX
			this.attempts[ this.currentThrow ] = [0 , -1];
		}
		else 
		{
            soundManager.playSuccessfulThrowSound();
			this.attempts[ this.currentThrow ] = [this.distance, this.formatScore(this.distance)];
		}
		this.changeState(GAME_STATE.WAITING);
		
		//increase
		this.currentThrow++;
		
		
		v.get('ingamehud').showThrowComplete();
	},
    backToMenu:function()
    {
        TweenLite.killTweensOf(this);
    },
	finalizeEndThrow: function(){
		this.changeState(GAME_STATE.WALKING);
	
		v.get('ingamehud').updateAttempts(this.attempts);
		//is final throw?
		if(this.currentThrow >= GAME_CONST.maxThrows) {
            this.changeState(GAME_STATE.FINISHING);
            
    
            
			setTimeout(function(){
				v.get('ingame').transitionOut();
                v.get('ingamehud').transitionOut();
                v.get('menuhud').setState('nobuttons');
                v.get('endgameanim').transitionIn();
			}, 50);
        } else {
            //console.log('end throw setup next throw');
            game.setupNextThrow();   
        }
	},
	destroyObstacles: function(){
		for( var i = 0, l = this.obstacle_pool.length; i < l; i++ ) {
			this.obstacle_pool[0].destroy();
			this.obstacle_pool[0] = null;
			this.obstacle_pool.splice(0, 1);
		}
	},
    stop: function(){
        TweenLite.killTweensOf(this);
		this.xpos = 0;
    },
    updateHud: function(delta){
        var hud = v.get('ingamehud');
        hud.updateDistance(this.formatScore(this.distance));
        hud.updateWindSpeed(Math.round(this.wind.current));
        hud.updateStars(this.stars);
    },
	hasHit: false,
	update:function(delta) {
		var vp = app.viewPort,gs = app.guiSize;
        if(this.paused) return;

		switch( this.state )
		{
			case GAME_STATE.SWINGING:
				
				//accelerate power
				var k = this.keepyup;
				
				if(k.currentY >= (k.hitY - 70 - (k.speed*2)) && k.speed > 0) {
                   
					v.get('ingame').keepup();	
				}
				
				//if below hit point then hit the ball up
				if(k.currentY >= k.hitY) {
                    //
					//hit the ball up
					var speed = m.randomInt( k.minHitSpeed, k.maxHitSpeed );

					k.speed = -speed;
					k.lastHitSpeed = speed;
					
					k.rotationSpeed = speed * 0.005;
				}
				//dampen speed with gavity and apply new speed to position
				k.speed += k.gravity;
				k.currentY += k.speed;
				this.hasHit = false;
				
				///wind speed
				this.wind.current += this.wind.speed * delta;
				if((this.wind.current <= this.wind.target && this.wind.speed < 0) || (this.wind.current >= this.wind.target && this.wind.speed > 0) || this.wind.target == this.wind.current) {
					//this happens when the wind reaches it target (between -10 and 10) then change direction to reach its next target
                    if(this.wind.target < 0) {
						this.wind.target = 10;
						this.wind.speed = koko.math.randomInt(2, 5) * 0.03;
					} else { 
						this.wind.target = koko.math.randomInt(2, 8) * -1;
						this.wind.speed = koko.math.randomInt(2, 5) * -0.03;
					}
				}
			break;
			case GAME_STATE.WATCHING:
				var k = this.keepyup;
				if(!this.hasHit) {
                    
                
                   
                    
					k.speed += k.gravity;
					k.currentY += k.speed;
					
					
					
						if(k.currentY >= (k.hitY - 130 - (k.speed*3)) && k.speed > 0) {
							v.get('ingame').hit();	
						}
					
						
					if(k.currentY >= k.hitY) {
						k.currentY = k.hitY;
						this.hasHit = true;
					}
					return;
				}
				
				
				
				
				if(this.shouldSetDefaultObstacles && this.obstacle_pool.length > 1) {
					game.xpos+=5;	
					return; 
				}
				//gravity
				this.speed.y += GAME_CONST.gravity * delta;
				
				//wind to speed x
				this.wind.current *= 0.996; //wind gradually dies down //was 0.998
				this.speed.x += (this.wind.current*0.01*delta); //wind affects x speed //was *0.02
				
                //maximum speedz you ghet me
				if(this.speed.x > 50) this.speed.x = 50
				if(this.speed.y < -50) this.speed.y = -50;
				if(this.speed.y > 50) this.speed.y = 50;
				
				//apply force
				this.position.x += this.speed.x * delta;
				this.position.y += this.speed.y * delta;
				this.position.rotation += this.speed.rotation * delta;
				

                //throw behind foul throw
				if(this.position.x < GAME_CONST.foulThrowX) {
					this.endThrow();
					return;
				}
				
                //BOUNCY BOUNCY
				if (this.position.y > GAME_CONST.floorY) {
					
                    //put on floor height
					this.position.y = GAME_CONST.floorY;
					
                    //only bounce if fast enough speed && !dontBounce
					if ((this.speed.x > 2 || this.speed.y > 2) && !this.dontBounce) {
						
                        soundManager.playBounceSound();
                        
						//bounce y
						var randY = 0.55 + (Math.random() * 0.3);
						this.speed.y *= randY * -1;
						
						var randX = 0.8 + (Math.random() * 0.2);
						this.speed.x *= randX;
						
						
						var chance = Math.random();
						if(this.speed.x < 2) {
							this.speed.x = 0;
							this.speed.rotation *= 0.7;
							chance = 0.5; //force speed.y * 0.7;
						}
						
                        //random change for increased horizontal or vertical speed
						if(chance < 0.33) {
							this.speed.y *= 1.3; //1.2
							this.speed.x *= 0.7; //0.8
						} else if (chance < 0.66) {
							this.speed.y *= 0.7;
							this.speed.x *= 1.3;
						}
						
                        //rotate speed set on 
						this.speed.rotation = Math.min(0.5,((this.speed.x*0.05)+((this.speed.x*0.05)*Math.random()))*0.2);
                        //console.log(this.speed.rotation, this.speed.x);
					} else {
						this.position.rotation = 0;
						this.speed.x = 0;
						this.speed.y = 0;
						this.distance = (this.position.x - GAME_CONST.foulThrowX) - GAME_CONST.cameraFarX;
						this.endThrow();
						return;
					}
				}
				
                //disthance
				this.distance = (this.position.x - GAME_CONST.foulThrowX) - GAME_CONST.cameraFarX;
				if(this.distance < 0) this.distance = 0
                //move camera 
				this.xpos = Math.max(0, this.distance - GAME_CONST.distanceDragWidth);
				this.sortObstacles();
				this.sortStars();
			break;
		}
        
        this.updateHud(delta);
	},
	lastShownStar:0,
	sortStars: function(){
		var authorised = this.stars < ((this.currentThrow+1)*2) && this.lastShownStar + 3000 < new Date().getTime();
		
		if(authorised) {
			if(Math.random() < 0.0075){
                soundManager.playStarTabInAndOutSound();
				this.lastShownStar = new Date().getTime();
				v.get('ingamehud').showStar();
			}
		}
	},
    sortObstacles: function(){
        var vp = app.viewPort,gs = app.guiSize;
        //create a new obstacle
        if(this.obstacle_pool.length == 0 || this.obstacle_pool[this.obstacle_pool.length-1].pos.x < game.xpos + gs.width + 100) {
            var obs;
			
			var chance = (0.95 - (this.distance*0.000015));
			if(chance < .25) chance = 0.25;
			
			var isBoost = Math.random() < chance; //30% chance of boost, 70% change of obstacle
			
			var num = m.randomInt( isBoost ? 0 : 5, isBoost ? 4 : 7 );

			switch(num) {
				case 0:
					obs = new KinderinoBoost();
				break;
				case 1:
					obs = new ManholeBoost();
				break;
				case 2:
					obs = new MoleBoost();
				break;
				case 3:
					obs = new ParasolBoost();
				break;
				case 4:
					obs = new NetBoost();
				break;
				case 5:
					obs = new GymBagObstacle();
				break;
				case 6:
					obs = new ChairObstacle();
				break;
				case 7:
					obs = new BinObstacle();
				break;
			}

            //position and add into the mix
            var lastPos = this.obstacle_pool.length == 0 ? 0 : this.obstacle_pool[ this.obstacle_pool.length-1 ].pos.x;
            obs.setPosition(lastPos +  m.randomInt(1500, 4500));
			
            v.get('ingame').addObstacle(obs);

            this.obstacle_pool.push(obs);
        }

        //sdfdsf
        for(var i = 0; i < this.obstacle_pool.length; i++)
        {
            var itm = this.obstacle_pool[i];

            itm.update();

            if(((itm.pos.x+itm.pos.width) < this.xpos) || ((this.xpos + vp.width) < itm.pos.x) || itm.hasCollided) continue;

            if(this.position.x > itm.pos.x && this.position.x < (itm.pos.x + itm.pos.width) && this.position.y > itm.pos.y  && this.position.y < (itm.pos.y + itm.pos.height)) {
                itm.hasCollided = true;

                itm.collide();

                if(itm.soundRef.length > 0) {
                    au.play(itm.soundRef);	
                }

                ////console.log(this.speed);
                //impact this this
                this.speed.y *= itm.speedImpact.y;
                this.speed.x *= itm.speedImpact.x;
				
				if(itm.speedImpact.x <= 0.1) this.speed.rotation *= 0.3;
				
                //if speed.y is positive, flip reverse it.
                if(this.speed.y > 0 && itm.speedImpact.y !=1) this.speed.y *= -1;
                //if speed.y > minSpeed, se to min speed.y
                if(this.speed.y > itm.minSpeedImpact.y) this.speed.y = itm.minSpeedImpact.y;
                //if speed.x < minSpeed.x, set to min speeed.x]
                if(this.speed.x < itm.minSpeedImpact.x) this.speed.x = itm.minSpeedImpact.x;
                //stop dat bounce ting
                if(this.speed.y == 0 && this.speed.x == 0) this.dontBounce = true;
                ////console.log(this.speed);
                //if(this.speed.y > itm.minSpeed.y) this.speed.y = itm.minSpeed.y;
                //if(this.speed.x < itm.minSpeed.x) this.speed.x = itm.minSpeed.x;
            }
        }   
    },
	tweenPosition: function(duration, xMove, ease) {
		var _e = ease ? ease : Linear.easeNone;
		TweenLite.to(this, duration, { xpos: this.xpos + xMove, ease:_e });
	}
});
//update listener
var game = new gameController();