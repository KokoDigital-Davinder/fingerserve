koko.class('CharacterSelectView', 'view', {
    CharacterSelectView: function() {
        this.superr.view.call(this);
    },
    lockButtonPress: false,
    buildView: function() {
        var t = this, g = app.guiSize, vp = app.viewPort;
       
		this.slideWidth = 400;
		
        this.children = koko.display.buildLayout([
			
			{ name:'title_container', type:'container', id:'SelectFairyTitle', x:g.width*0.5, y:20 },
			{ name:'title_bg', type:'sprite', id:'WFTitle', parent:'title_container', x:0, y:0, regX:238 },
			{ name:'title_txt', type:'bmptext', parent:'title_container', text:lang.getTerm('selectdog'), fitWidth:386, size:'45px', font:lang.getFontName(), tint:0xf24321, y:13, align:'center' },
			
			{ name:'slider_cont', type:'container', x:vp.centerX, y:vp.centerY+180 },
			{ name:'slider', type:'container', parent:'slider_cont' },
			{ name:'chars', type:'container', parent:'slider' },
				
				{ name:'char1', type:'container', parent:'chars' },
				{ name:'char1_shadow', type:'sprite', id:'SelectShadow', parent:'char1', regX:160, regY:23, y: 20 },
				{ name:'char1_bmp', type:'sprite', id:'HandBlue', parent:'char1', regX:179, regY:410 },


				{ name:'char2', type:'container', x:this.slideWidth, parent:'chars' },
				{ name:'char2_shadow', type:'sprite', id:'SelectShadow', parent:'char2', regX:160, regY:23, y: 20 },
				{ name:'char2_bmp', type:'sprite', id:'HandGreen', parent:'char2', regX:179, regY:410 },


				{ name:'char3', type:'container', x:this.slideWidth*2, parent:'chars' },
				{ name:'char3_shadow', type:'sprite', id:'SelectShadow', parent:'char3', regX:160, regY:23, y: 20 },
				{ name:'char3_bmp', type:'sprite', id:'HandOrange', parent:'char3', regX:179, regY:410 },


				{ name:'char4', type:'container',  x:this.slideWidth*3, parent:'chars' },
				{ name:'char4_shadow', type:'sprite', id:'SelectShadow', parent:'char4', regX:160, regY:23, y: 20 },
				{ name:'char4_bmp', type:'sprite', id:'HandRed', parent:'char4', regX:179, regY:410 },
			
			{ name:'navigation', x:vp.centerX, y: vp.bottom },
			{ name:'next_btn', type:'sprite', id:'NextButton', x:50, y:-60, regX:45, regY:45, parent:'navigation'  },
			{ name:'prev_btn', type:'sprite', id:'PreviousButton', x:-50, y:-60, regX:45, regY:45, parent:'navigation' },

			
        ], this.container);
		
		 UNLOCKABLES.calculate();
        // work out which characters should be unlocked with star balance
        
		
		this.grayScaleFilter = new PIXI.filters.GrayFilter();
		this.colorFilter = new PIXI.filters.ColorMatrixFilter();
		this.colorFilter.brightness(0.4);
		
		this.sortOutCharacterContainer();
		
		var prev_btn = new Button(this.children.prev_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound(); 
				this.slider.gotoPreviousSlide(.2);
			}.bind(this)
		});
		
		var next_btn = new Button(this.children.next_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound(); 
				this.slider.gotoNextSlide(.2);
			}.bind(this)
		});
		
		var select_btn = new Button(this.children.chars, null, {
			click: function(){
                
				this.select();
			}.bind(this)
		});
		this.children.slider.hitArea = new PIXI.math.Rectangle(-vp.centerX, -vp.centerY-130, g.width + (this.slideWidth*4), g.height);
		
		
		this.slider = new DragSliderContainer(this.children.slider, 'x', 0, this.slideWidth, 3, {
			update: function(drag){
				var perc = drag.indexPerc;
				for( var i = 0; i < this.children.chars.children.length; i++ ){
					var s = 1;
					var diff = Math.abs(i - perc)*0.3;
					s-= diff;
					d.applyProps(this.children.chars.children[i], { scale: s});
				}
			}.bind(this)
		});
		this.slider.gotoSlide(0);
		
		this.elements = [prev_btn, next_btn, select_btn, this.slider];
		
		app.addResizeHandler('charselect', this.resize.bind(this));
    },
	select: function() {
		if(this.slider.moving || this.lockButtonPress) return;
		if(UNLOCKABLES.chars[this.slider.index].locked) {
             soundManager.playLockedSound(); 
			var char = this.children.chars.children[this.slider.index];
			this.lockButtonPress = true;
			d.killAnimations(char.children[char.children.length-3]);
			d.killAnimations(char.children[char.children.length-2]);
			d.killAnimations(char.children[char.children.length-1]);
			
			d.applyProps(char.children[char.children.length-3], { scale:1, rotation:0 });
			d.applyProps(char.children[char.children.length-2], { scale:1 });
			d.applyProps(char.children[char.children.length-1], { scale:1 });
			
			d.animateObject(char.children[char.children.length-3], .15, { scale:1.2, rotation:-33 }); //graphics must be at 30 angle
			d.animateObject(char.children[char.children.length-3], .05, { delay:0.2, rotation:-27, yoyo:true, repeat:9 });
			d.animateObject(char.children[char.children.length-3], .15, { delay:0.75, scale:1, rotation:0  });
			
			d.animateObject(char.children[char.children.length -2], .2, { scale:1.1, yoyo:true, repeat:3 });
			d.animateObject(char.children[char.children.length -1], .2, { scale:1.1, yoyo:true, repeat:3 });
			setTimeout(function(){this.lockButtonPress = false}.bind(this), 1250)
			return;
		}
		soundManager.playButtonSound(); 
		user.char = this.slider.index;
		
		this.transitionOut('story');
	},
	sortOutCharacterContainer: function() {
		var c = this.children;
		var chars = [ c.char1, c.char2, c.char3, c.char4 ];

		for( var i = 0; i < chars.length; i++ ){
			var isLocked = UNLOCKABLES.chars[i].locked;
			var char = chars[i];
			
			if(isLocked) {
				char.children[1].filters = [ this.grayScaleFilter, this.colorFilter ];
								
				var diff = -8;
				var ydiff = -160;
				ydiff = -50;
				d.buildLayout([
					//{ name:'lock_shadow', type:'sprite', id:'LockedShadow', regX:62, regY:74, y:130+ydiff, x:-20+diff },
					{ name:'lock', type:'sprite', id:'LockedShut', regX:62, regY:74, y:-30+ydiff, x:-20+diff },
					{ name:'stars_bg', type:'sprite', id:'StarsTotalBack', regX:40, regY:40, y:20+ydiff, x:60+diff },
					{ name:'stars_txt', type:'bmptext', text:UNLOCKABLES.chars[i].stars.toString(), size:'40px', font:lang.getFontName(), tint:0xffffff, align:'center', y:-2+ydiff, x:57+diff },
				], char);
				
			} else{
				char.children[1].filters = null;	
				d.applyProps(char, { scale:1 }, true);
			}
		}
		
	},
	resize: function(){
		var vp = app.viewPort;
		
		this.children.title_container.y = vp.top+20;
		this.children.navigation.y = vp.bottom;
		
	},
    transitionIn: function() {
        var vp = app.viewPort;
		
		this.buildView();
        this.show();
		this.enable();
		
        this.slider.gotoSlide(0);
        
		
		d.applyThenAnimate(this.container, { alpha:0 }, .3, { alpha:1 });
		d.applyThenAnimate(this.children.title_container, { alpha:0, y: vp.top }, .3, { delay:.2, y:vp.top+20, alpha:1 });
		
		v.get('menuhud').setState('menus');
		v.get('menuhud').showBackButton();
    },
    transitionOut: function(to) {
		this.grayScaleFilter = null;
		this.colorFilter = null;
		
		d.animate(this.container, .2, { alpha:0, onComplete: function(){
			this.destroyView();
			this.hide();
			v.get(to).transitionIn();
		}.bind(this) });
		
		
		app.removeResizeHandler('charselect', this.resize.bind(this));
		
        this.disable();
    },
});