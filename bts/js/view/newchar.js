koko.class('NewCharView', 'view', {
    NewCharView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var t = this, g = app.guiSize, vp = app.viewPort;
		
        this.children = koko.display.buildLayout([	
			{ name:'char', type:'container', x:vp.centerX, y:vp.centerY},
			{ name:'char_shadow', type:'sprite', id:'SelectShadow', parent:'char', regX:160, regY:23, y: 140, x: 10 },
			{ name:'char_bmp', type:'sprite', id:UNLOCKABLES.chars[this.unlocked].spriteName, parent:'char', regX:209, regY:154 },
			
			{ name:'lock', parent:'char', regX:70, regY:85, y:85, x:0 },
			{ name:'lock_locked', type:'sprite', id:'LockedShut', parent:'lock'  },
			{ name:'lock_unlocked', type:'sprite', id:'LockedOpen', parent:'lock', visible:false }
        ], this.container);
		
		this.grayScaleFilter = new PIXI.filters.GrayFilter();
		this.colorFilter = new PIXI.filters.ColorMatrixFilter();
		this.colorFilter.brightness(0.4);
		
		var char = this.children.char;
		
		char.children[1].filters = [ this.grayScaleFilter, this.colorFilter ];
    },
    transitionIn: function(menuOut, unlocked){
        this.unlocked = unlocked;
		
		this.buildView();
        this.show();
		
		v.get('menuhud').setState('menus');
		
		setTimeout(function(){
			this.children.lock_locked.visible = false;
			this.children.lock_unlocked.visible = true;
			
			d.animateObject(this.children.lock, .7, { alpha:0, scale:2 });
			
			var brightness = { val: 0.4 };
			
			this.children.char.children[1].filters = [ this.colorFilter ];
			
			TweenMax.to(brightness, .3, { val:1, onUpdate:function(){
				this.colorFilter.brightness(brightness.val);
			}.bind(this)});
			
		}.bind(this), 1000);

		d.applyThenAnimate(this.container, { alpha:1}, 1, { delay:3, alpha:0, onComplete:function(){
			this.transitionOut();
		}.bind(this)});
		
		this.menuOut = menuOut;
    },
    transitionOut: function(){
        this.destroyView();  
        this.hide();
			
		d.animate(this.container, .2, { alpha:0, onComplete: function(){
			v.get(this.menuOut).transitionIn();
		}.bind(this)});
	}
});