var _gsScope = (typeof(module) !== "undefined" && module.exports && typeof(global) !== "undefined") ? global : this || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push( function() {
	//ignore the line above this and at the very end - those are for ensuring things load in the proper order
	"use strict";
	_gsScope._gsDefine.plugin({
		propName: "pixiPlugin", //the name of the property that will get intercepted and handled by this plugin (obviously change it to whatever you want, typically it is camelCase starting with lowercase).
		priority: 0, //the priority in the rendering pipeline (0 by default). A priority of -1 would mean this plugin will run after all those with 0 or greater. A priority of 1 would get run before 0, etc. This only matters when a plugin relies on other plugins finishing their work before it runs (or visa-versa)
		API: 2, //the API should stay 2 - it just gives us a way to know the method/property structure so that if in the future we change to a different TweenPlugin architecture, we can identify this plugin's structure.
		version: "1.0.0", //your plugin's version number
		overwriteProps: [], //an array of property names whose tweens should be overwritten by this plugin. For example, if you create a "scale" plugin that handles both "scaleX" and "scaleY", the overwriteProps would be ["scaleX","scaleY"] so that if there's a scaleX or scaleY tween in-progress when a new "scale" tween starts (using this plugin), it would overwrite the scaleX or scaleY tween.
		init: function(target, value, tween) {
			this._target = target; //we record the target so that we can refer to it in the set method when doing updates.

			if(value.position && value.position.x !== undefined) this._addTween(target.position, "x", target.position.x, value.position.x, "x", false);
			if(value.position && value.position.y !== undefined) this._addTween(target.position, "y", target.position.y, value.position.y, "y", false);

			if(value.scale && value.scale.x !== undefined) this._addTween(target.scale, "x", target.scale.x, value.scale.x, "x", false);
			if(value.scale && value.scale.y !== undefined) this._addTween(target.scale, "y", target.scale.y, value.scale.y, "y", false);

			if(value.pivot && value.pivot.x !== undefined) this._addTween(target.pivot, "x", target.pivot.x, value.pivot.x, "x", false);
			if(value.pivot && value.pivot.y !== undefined) this._addTween(target.pivot, "y", target.pivot.y, value.pivot.y, "y", false);

			if(value.alpha !== undefined)this._addTween(target, "alpha", target.alpha, value.alpha, "alpha", false);
			
			if(value.width !== undefined) this._addTween(target, "width", target.width, value.width, "width", false);
			if(value.height !== undefined) this._addTween(target, "height", target.height, value.height, "alpha", false);

			if(value.rotation !== undefined) this._addTween(target, "rotation", target.rotation, value.rotation, "rotation", false);

			return true;
		}

	});

}); if (_gsScope._gsDefine) { _gsScope._gsQueue.pop()(); }