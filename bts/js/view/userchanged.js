koko.class('UserChangedView', 'view', {
	UserChangedView: function(){
		this.superr.view.call(this);
	},
	buildView: function(){
        if(koko.assets.getAssetURL('av'+user.id))
        {
            var vp = app.viewPort;
            var texture = PIXI.Texture.fromImage(koko.assets.getAssetURL('av'+user.id), true);
            var avatarScale = (138 / texture.width) * .48;
            this.children = d.buildLayout([
                { name:'NowPlayingAsBack', type:'sprite', id:'UserLoggedInBack' },
                { name:'NowPlayingAsTextfield', type:lang.textType(), font:lang.getFontName(true), text:lang.getTerm('playingas').replace('|NN|', lang.reverseString(user.name)), size:'37px',  tint:0xffffff, x: 100, y: 35, fitWidth:250, visible:!user.unsupportedChars },
			{ name:'NowPlayingAsTextfield', type:'text', text:lang.getTerm('playingas').replace('|NN|', lang.reverseString(user.name)), size:'37px',  tint:0xffffff, x: 100, y: 35, fitWidth:250, visible:user.unsupportedChars },
            { name:'NowPlayingAsImage', type:'bitmap', id:'av'+user.id, x:24, y:18, scale:avatarScale }
            ], this.container);

            //position
            this.container.x = vp.centerX;
            this.container.y = vp.bottom-100;

            //pivot
            this.container.pivot.x = 191;
            this.container.pivot.y = 52;
        }
	},
	transitionIn: function(){
		this.buildView();
		this.show();
		d.applyThenAnimate(this.container, { delay:0.5, alpha: 0 }, .3, { alpha: 1});
		this.transitionOut();
	},
    transitionOut: function(){
		d.applyThenAnimate(this.container, { alpha: 1 }, .3, { delay:1.5, alpha: 0, onComplete:function(){
			this.destroyView();  
			this.hide();
		}.bind(this)});
    }
});