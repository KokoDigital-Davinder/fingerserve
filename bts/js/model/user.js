/****************************************************************************
KINDER GAMIFICATION PARAMATERS
****************************************************************************/
var skipLoadGamification = false;
var content_id = '';   
var avatar_list = ''; 
var current_avatar_id = '';
var current_avatar_name;
var current_avatar_image_url;
var language;
var country;
var debug;
var appLoaded = false;

function gamificationParameters($content_id, $avatar_list, $current_avatar_id, $current_avatar_name, $current_avatar_image_url,$language,$country,$debug)
{
     content_id = $content_id;
    avatar_list = JSON.parse($avatar_list);
    current_avatar_id = $current_avatar_id;
    current_avatar_name = $current_avatar_name;
    current_avatar_image_url = $current_avatar_image_url;
    language =  new languageController($language.substring(0, 2));
    lang = new languageController($language.substring(0, 2));
    country = $country;
    debug = $debug;
  
    if(!$avatar_list || $avatar_list == undefined || $avatar_list.length == 0 || $avatar_list == null || $avatar_list === null || $avatar_list == 'null' )
    {
        var guestAvatar = {}; 
        guestAvatar.avatar_id = 998; 
        guestAvatar.avatar_image = "img/avatars/av_999.png"; 
        guestAvatar.avatar_name = lang.getTerm('guest')+' 1';
        avatar_list = {};
        avatar_list.avatars = [guestAvatar];
        
        current_avatar_id = 998;
        current_avatar_name = lang.getTerm('guest')+' 1';
        current_avatar_image_url = "img/avatars/av_999.png"; 
    };
    
    //v.get('mainmenu').setConsoleText("LOADED");
    skipLoadGamification = true;
    setTimeout(function()
    {
        main.userLoaded();
    }, 1000)
    

       //v.get('mainmenu').setConsoleText(avatar_list);
}

/***************************************************************************************************
KOKO USER CLASS
***************************************************************************************************/
koko.class('UserModel', '', {
    loadedAvatar: [],
    avatarAssets: [],
    GUEST:999,
	activeUserName: null,
	userNames: null,
	users: null,
    ids:null,
    highScore: null,
    score: null,
    attempts: null,
    totalstars:0,
    stars: 0,
	dog:0,
    id:0,
    unsupportedChars:false,
	name:'',
    audio: {
        music: 0,
        sfx: 0
    },
    UserModel: function(){
        this.userNames = [];
        this.ids = [];
        this.users = [];
        
        this.score = [0, 0, 0];
        this.highScore = [0, 0, 0];
    },
    load:function() 
    {
        /****************************************************
        LOAD USER DATA
        ****************************************************/
        if(!kokodebug)
        {
            if(!skipLoadGamification) 
            {
                location.href = 'http://contentgamificationparameters/' ;
                //Redirect to Kinders Gamification version 
            }
        }
        else
        {
            //Debug is true so load Koko's JSON for testing purposes
            //this.getKokoJSON('http://kokodev.co.uk/kinder/avatar.json');
            //lang = new languageController('pl');//
               gamificationParameters(null,null,null,null,null,'en',null,1);
        }
    }, 
    
    
    
    getKokoJSON: function ($URL)
    {
        /****************************************************
        LOAD KOKO DEBUG AVATAR DATA
        ****************************************************/
        var Httpreq = new XMLHttpRequest();
        var t = this;
        Httpreq.onload = function(){
			if (Httpreq.readyState === 4) 
            { 
				var data = JSON.parse(Httpreq.responseText);
				if (Httpreq.status === 200) 
                {  
					avatar_list = data;
                                        current_avatar_id = avatar_list.avatars[0].avatar_id;
                    current_avatar_name = avatar_list.avatars[0].avatar_name;
                    current_avatar_image_url = avatar_list.avatars[0].avatar_image;
                    //main.userLoaded();	     
				} 
			}
		}
        Httpreq.open("GET",$URL,true);
        Httpreq.send(null);    
    },
    
    buildPlayerList: function($data)
    {  
        this.loadedAvatar = $data.avatars;
        var t = this;
        var guestAvatar = {}; 
        guestAvatar.avatar_id = this.GUEST; 
        guestAvatar.avatar_image = "img/avatars/av_999.png"; 
        
        if(current_avatar_name == lang.getTerm('guest')+' 1')
        {
            guestAvatar.avatar_name = lang.getTerm('guest')+' 2';
        }
        else
        {
             guestAvatar.avatar_name = lang.getTerm('guest');
        }
        
        this.loadedAvatar.push(guestAvatar); 
        for(var i=0; i<this.loadedAvatar.length; i++)
        {
            this.avatarAssets.push({id:'av'+this.loadedAvatar[i].avatar_id, src:this.loadedAvatar[i].avatar_image});
            this.userNames.push(this.loadedAvatar[i].avatar_name);
            this.ids.push(this.loadedAvatar[i].avatar_id);  
            if(!lang.hasChars(this.loadedAvatar[i].avatar_name)) this.unsupportedChars = true;
        }
        
        this.loadUsers();
       //kokodebug ? this.selectUser(this.loadedAvatar[0].avatar_id) : this.selectUser(current_avatar_id); 
        this.selectUser(this.loadedAvatar[0].avatar_id);
        //v.get('menuhud').setState('mainmenu');
		t.loadSettings();
        koko.assets.clearManifest();
        koko.assets.addToManifest(t.avatarAssets);  
        koko.assets.loadManifest(function(){}, function(){setTimeout(function(){t.showPlayingAsAfterLoad()},300);}); //v.get('genericHUD').showPlayingAs();
       
    },
    showPlayingAsAfterLoad: function()
    {
        for(var i=0; i<this.loadedAvatar.length; i++)
        {
            var texture = PIXI.Texture.fromImage(koko.assets.getAssetURL('av'+this.loadedAvatar[i].avatar_id), true);
        }
        setTimeout(function(){v.get('userloggedin').transitionIn()},500)
        
    },
    
    
    
    findAvatarByID:function($ID)
    { 
        for(var i=0; i<this.loadedAvatar.length; i++)
        {
            if(this.loadedAvatar[i].avatar_id == $ID) 
                return i; 
        }
    },
	loadSettings: function(){
		var u = co.get(this.cookieNamePrefix);
		if(u) {
            var data = JSON.parse(u);
            this.audio.music = data.music;
            this.audio.sfx = data.sfx;
		}
	},
	saveSettings: function(){
        co.set(this.cookieName, JSON.stringify({
            music: this.audio.music,
            sfx: this.audio.sfx
        }), 9999);
    },
    saveAttempts: function(att, stars, difficulty) {
        this.stars = stars;
        this.totalstars += stars;
        this.score[difficulty] = 0;
		
        this.attempts = [[0,0], [0,0], [0,0]];
		var highest = this.attempts[0];
        for( var i = 0; i < att.length; i++ ) {
            this.attempts[i][0] = att[i][0];
            this.attempts[i][1] = att[i][1];
            
			if(att[i][0] > this.highScore[difficulty]) this.highScore[difficulty] = att[i][0];   
			if(highest[0] < this.attempts[i][0]) highest = this.attempts[i];
        }
		this.score[difficulty] = highest[0];
        this.saveUser();
    },
    cookieNamePrefix: 'user_fingerserve_v00015',
	cookieName: function(name) {
		return this.cookieNamePrefix + name.replace(' ','');	
	},
    //This is really hacky, assumes that ID's is the same length, sorry, limited time =(
	loadUsers: function(){
        this.users = []; 
		for( var i = 0; i < this.userNames.length; i++ ){
            this.loadUser(this.userNames[i], this.ids[i]);
		}
	},
    loadUser: function(name, id){
        var u = co.get(this.cookieName(name+id));
        if(u) {
            var data = JSON.parse(u);
			this.users.push({
                id: data.id,
				name: name,
				highScore: data.highScore,
				totalstars: data.totalstars
			});
        } else {
			this.users.push({
                id: id, 
				name: name,
				highScore: [0, 0, 0],
				totalstars: 0
			});
		}
    },
	selectUser: function(id){
		var u = null;
		for( var i =0; i < this.users.length; i++ ){
			if(this.users[i].id == id) u = this.users[i];
		}
        this.id = u.id; 
		this.name = u.name;
		this.highScore = u.highScore;
		this.totalstars = u.totalstars;
        this.activeUserName = this.name;
        UNLOCKABLES.calculate();
	},
	saveUser: function() {
		co.set(this.cookieName(this.activeUserName+this.id), JSON.stringify({
            id:this.id, 
            highScore:this.highScore,
            totalstars: this.totalstars
        }), 9999);
        this.loadUsers(); 
	}
});
var user = new UserModel();