koko.class('UserModel', '', {
	activeUserName: null,
	userNames: null,
	users: null,
    UserModel: function(userNames){
		this.userNames = userNames;
		this.users = [];
        this.score = [0, 0, 0];
        this.highScore = [0, 0, 0];
        this.loadUsers();
		this.loadSettings();
    },
    highScore: null,
    score: null,
    attempts: null,
    totalstars:0,
    stars: 0,
	char:0,
	name:'',
    audio: {
        music: 0,
        sfx: 0
    },
	loadSettings: function(){
		var u = co.get(this.cookieNamePrefix);
		if(u) {
            var data = JSON.parse(u);
            this.audio.music = data.music;
            this.audio.sfx = data.sfx;
		}
	},
	saveSettings: function(){
        co.set(this.cookieName, JSON.stringify({
            music: this.audio.music,
            sfx: this.audio.sfx
        }), 9999);
    },
    saveAttempts: function(att, stars, difficulty) {
        this.stars = stars;
        this.totalstars += stars;
        this.score[difficulty] = 0;
		
        this.attempts = [[0,0], [0,0], [0,0]];
		var highest = this.attempts[0];
        for( var i = 0; i < att.length; i++ ) {
            this.attempts[i][0] = att[i][0];
            this.attempts[i][1] = att[i][1];
            
			if(att[i][0] > this.highScore[difficulty]) this.highScore[difficulty] = att[i][0];   
			if(highest[0] < this.attempts[i][0]) highest = this.attempts[i];
        }
		this.score[difficulty] = highest[0];
        this.saveUser();
    },
    cookieNamePrefix: 'user_fingerserve_v00013',
	cookieName: function(name) {
		return this.cookieNamePrefix + name.replace(' ','');	
	},
	loadUsers: function(){
		this.users = [];
		for( var i = 0; i < this.userNames.length; i++ ){
			this.loadUser(this.userNames[i]);
		}
	},
    loadUser: function(name){
        var u = co.get(this.cookieName(name));
        if(u) {
            var data = JSON.parse(u);
			this.users.push({
				name: name,
				highScore: data.highScore,
				totalstars: data.totalstars
			});
        } else {
			this.users.push({
				name: name,
				highScore: [0, 0, 0],
				totalstars: 0
			});
		}
    },
	selectUser: function(name){
		this.activeUserName = name;
		var u = null;
		for( var i =0; i < this.users.length; i++ ){
			if(this.users[i].name == name) u = this.users[i];
		}
		this.name = u.name;
		this.highScore = u.highScore;
		this.totalstars = u.totalstars;
		this.char = u.char;
	},
	saveUser: function() {
		co.set(this.cookieName(this.activeUserName), JSON.stringify({
            highScore:this.highScore,
            totalstars: this.totalstars,
			char: this.char
        }), 9999);
		this.loadUsers();
	}
	
});
var user = new UserModel(['Rory', 'كيفية ', 'ак игр', '戏方', 'רא', 'Steve', 'grać']);
user.selectUser(user.userNames[m.randomInt(0, 6)]);