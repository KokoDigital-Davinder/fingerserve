<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.3.4</string>
        <key>fileName</key>
        <string>//KOKO-NAS/Qweb/kinderfingerserve/_textures/rackets.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename>../img/rackets.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../img/rackets.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Crop</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>rackets/racket_3_handle_blue.png</filename>
            <filename>rackets/hand_1.png</filename>
            <filename>rackets/hand_2.png</filename>
            <filename>rackets/racket_1_end.png</filename>
            <filename>rackets/racket_1_handle_blue.png</filename>
            <filename>rackets/racket_2_end.png</filename>
            <filename>rackets/racket_2_handle_blue.png</filename>
            <filename>rackets/racket_3_end.png</filename>
            <filename>rackets/finger_2_blue_3.png</filename>
            <filename>rackets/finger_2_blue_4.png</filename>
            <filename>rackets/finger_2_blue_5.png</filename>
            <filename>rackets/finger_2_blue_9.png</filename>
            <filename>rackets/finger_2_blue_10.png</filename>
            <filename>rackets/finger_1_blue_1.png</filename>
            <filename>rackets/finger_1_blue_5.png</filename>
            <filename>rackets/finger_1_blue_7.png</filename>
            <filename>rackets/finger_1_blue_8.png</filename>
            <filename>rackets/finger_2_blue_1.png</filename>
            <filename>rackets/finger_2_blue_2.png</filename>
            <filename>rackets/hand_2.png</filename>
            <filename>rackets/racket_1_end.png</filename>
            <filename>rackets/racket_1_handle_blue.png</filename>
            <filename>rackets/racket_1_handle_green.png</filename>
            <filename>rackets/racket_1_handle_orange.png</filename>
            <filename>rackets/racket_1_handle_red.png</filename>
            <filename>rackets/racket_2_end.png</filename>
            <filename>rackets/racket_2_handle_blue.png</filename>
            <filename>rackets/racket_2_handle_green.png</filename>
            <filename>rackets/racket_2_handle_orange.png</filename>
            <filename>rackets/racket_2_handle_red.png</filename>
            <filename>rackets/racket_3_end.png</filename>
            <filename>rackets/racket_3_handle_blue.png</filename>
            <filename>rackets/racket_3_handle_green.png</filename>
            <filename>rackets/racket_3_handle_orange.png</filename>
            <filename>rackets/racket_3_handle_red.png</filename>
            <filename>rackets/finger_1_blue_1.png</filename>
            <filename>rackets/finger_1_blue_5.png</filename>
            <filename>rackets/finger_1_blue_7.png</filename>
            <filename>rackets/finger_1_blue_8.png</filename>
            <filename>rackets/finger_1_green_1.png</filename>
            <filename>rackets/finger_1_green_5.png</filename>
            <filename>rackets/finger_1_green_7.png</filename>
            <filename>rackets/finger_1_green_8.png</filename>
            <filename>rackets/finger_1_orange_1.png</filename>
            <filename>rackets/finger_1_orange_5.png</filename>
            <filename>rackets/finger_1_orange_7.png</filename>
            <filename>rackets/finger_1_orange_8.png</filename>
            <filename>rackets/finger_1_red_1.png</filename>
            <filename>rackets/finger_1_red_5.png</filename>
            <filename>rackets/finger_1_red_7.png</filename>
            <filename>rackets/finger_1_red_8.png</filename>
            <filename>rackets/finger_2_blue_1.png</filename>
            <filename>rackets/finger_2_blue_2.png</filename>
            <filename>rackets/finger_2_blue_3.png</filename>
            <filename>rackets/finger_2_blue_4.png</filename>
            <filename>rackets/finger_2_blue_5.png</filename>
            <filename>rackets/finger_2_blue_9.png</filename>
            <filename>rackets/finger_2_blue_10.png</filename>
            <filename>rackets/hand_1.png</filename>
            <filename>rackets/finger_2_green_9.png</filename>
            <filename>rackets/finger_2_green_10.png</filename>
            <filename>rackets/finger_2_green_1.png</filename>
            <filename>rackets/finger_2_green_2.png</filename>
            <filename>rackets/finger_2_green_3.png</filename>
            <filename>rackets/finger_2_green_4.png</filename>
            <filename>rackets/finger_2_green_5.png</filename>
            <filename>rackets/finger_2_orange_9.png</filename>
            <filename>rackets/finger_2_orange_10.png</filename>
            <filename>rackets/finger_2_orange_1.png</filename>
            <filename>rackets/finger_2_orange_2.png</filename>
            <filename>rackets/finger_2_orange_3.png</filename>
            <filename>rackets/finger_2_orange_4.png</filename>
            <filename>rackets/finger_2_orange_5.png</filename>
            <filename>rackets/finger_2_red_9.png</filename>
            <filename>rackets/finger_2_red_10.png</filename>
            <filename>rackets/finger_2_red_1.png</filename>
            <filename>rackets/finger_2_red_2.png</filename>
            <filename>rackets/finger_2_red_3.png</filename>
            <filename>rackets/finger_2_red_4.png</filename>
            <filename>rackets/finger_2_red_5.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
