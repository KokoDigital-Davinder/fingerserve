koko.class('InGameHUDView', 'view', {
    InGameHUDView: function(){
        this.superr.view.call(this);
    },
    buildView: function(){
        var g = app.guiSize, vp = app.viewPort;
		
        this.children = koko.display.buildLayout([
			
			{ name:'top_container', x:vp.centerX, y: vp.top },
			//DISTANCE PANEL - done
			{ name:'distance_cont', type:'container', x:43, y:10, parent:'top_container' },
			{ name:'distance_bg', type:'sprite', id:'DistanceBack', parent:'distance_cont', x:0, y:0 },
			{ name:'distance_title', type:'bmptext', parent:'distance_cont', x:10, y:10, fitWidth:120, text:lang.getTerm('distance'), size:'22px', font:lang.getFontName(), tint:0xf24321 },
			{ name:'distance_txt', type:'bmptext', parent:'distance_cont', x:10, y:30, fitWidth:120, text:'0', size:'46px', font:lang.getFontName(), tint:0x999999},//////////////////////////////////////////////// top of screen

			//WIND SPEED - done
			{ name:'wind_cont', type:'container', x:-179, y:10, parent:'top_container' },
			{ name:'wind_bg', type:'sprite', id:'WindSpeedBack', parent:'wind_cont' },
			{ name:'wind_title', type:'bmptext', parent:'wind_cont', x:85, y:10, fitWidth:120, text:lang.getTerm('windspeed'), size:'22px', font:lang.getFontName(),tint:0xf24321 },
			{ name:'wind_speed_txt', type:'bmptext', parent:'wind_cont', x:85, y:30, text:'+10', size:'46px', font:lang.getFontName(), tint:0x999999 },
			{ name:'windmph_title', type:'bmptext', text:lang.getTerm('mph'), size:'28px', fitWidth:70, font:lang.getFontName(), tint:0x999999, parent:'wind_cont', x:140, y:43, },
			{ name:'wind_arrows', type:'container', id:'WindArrows', parent:'wind_cont', x:41, y:41, regX:31, regY:31 },
			{ name:'wind_arrow_good', type:'sprite', id:'WindArrowGood', parent:'wind_arrows' },
			{ name:'wind_arrow_bad', type:'sprite', id:'WindArrowBad', parent:'wind_arrows', visible:false },

			//ATTEMPT DISTANCE BUBBLE - done
			{ name:'attempt_popup_cont', type:'container', id:'AttemptDistanceBubble', x:568, y:200, visible:false },
			{ name:'attempt_popup_bg', type:'sprite', id:'AttemptDistanceBubbleBack', parent:'attempt_popup_cont', regX:145},
			{ name:'attempt_popup_title', type:'bmptext', parent:'attempt_popup_cont', y:20, fitWidth:176, text:lang.getTerm('youreached'), size:'24px', font:lang.getFontName(), tint:0x999999, align:'center' },
			{ name:'attempt_popup_distance_txt', type:'bmptext', parent:'attempt_popup_cont', fitWidth:176, y:50, text:'250', size:'80px', font:lang.getFontName(), tint:0xa6e1, align:'center'},/////////////////////// You reached ...

			//FOUL THROW - done
			{ name:'foul_throw_bubble', type:'container',  x:568, y:200, visible:false },
			{ name:'foul_throw_bg', type:'sprite', id:'FoulThrowBubbleBack', parent:'foul_throw_bubble', x:0, y:0, regX:145 },
			{ name:'foul_throw_title', type:'bmptext', parent:'foul_throw_bubble', y:50, fitWidth:176, text:lang.getTerm('foulthrow'), size:'36px', font:lang.getFontName(), align:'center', tint:0x999999, x:0 },

			//STAR COUNT - done
			{ name:'stars_collected_cont', type:'container', x:vp.right - 10, y:vp.top + 10, regX:153 },
			{ name:'stars_collected_bg', type:'sprite', id:'StarsCollectedBack', parent:'stars_collected_cont' },
			{ name:'stars_collected_txt', type:'bmptext', parent:'stars_collected_cont',text:'00', size:'45px', x: 35, y: 15, align:'center', font:lang.getFontName(), tint:0xa6e1 },

			//THROW HUD
			{ name:'throw_hud', type:'container', x:vp.centerX, y: -200 },
			{ name:'ThrowHudBack', type:'sprite', id:'ThrowHudBack', parent:'throw_hud', regX:258 },

			{ name:'throw_info_1', parent:'throw_hud', y:-31, x:-226 },
			{ name:'throw_info_1_complete', type:'sprite', id:'AttemptBoneComplete', parent:'throw_info_1', x:15, y:45, visible:false },
			{ name:'throw_info_1_inactive', type:'sprite', id:'AttemptBoneInactive', parent:'throw_info_1', x:15, y:45 },
			{ name:'throw_info_1_distance_txt', type:'bmptext', parent:'throw_info_1', x:70, y:55, fitWidth:70, text:'--', size:'32px', font:lang.getFontName(), tint:0x999999},/////////// non active buttons at the bottom

			{ name:'throw_info_2', parent:'throw_hud', y:-31, x:-77 },
			{ name:'throw_info_2_complete', type:'sprite', id:'AttemptBoneComplete', parent:'throw_info_2', x:15, y:45, visible:false },
			{ name:'throw_info_2_inactive', type:'sprite', id:'AttemptBoneInactive', parent:'throw_info_2', x:15, y:45 },
			{ name:'throw_info_2_distance_txt', type:'bmptext', parent:'throw_info_2', x:70, y:55, fitWidth:70, text:'--', size:'32px', font:lang.getFontName(), tint:0x999999},

			{ name:'throw_info_3', parent:'throw_hud', y:-31, x:73 },
			{ name:'throw_info_3_complete', type:'sprite', id:'AttemptBoneComplete', parent:'throw_info_3', x:15, y:45, visible:false },
			{ name:'throw_info_3_inactive', type:'sprite', id:'AttemptBoneInactive', parent:'throw_info_3', x:15, y:45 },
			{ name:'throw_info_3_distance_txt', type:'bmptext', parent:'throw_info_3', x:70, y:55, fitWidth:70, text:'--', size:'32px', font:lang.getFontName(), tint:0x999999},

			{ name:'throw_attempt', type:'container', parent:'throw_hud', x:-226, y:-31 },
			{ name:'throw_attempt_bg', type:'sprite', id:'ThrowAttemptBack', parent:'throw_attempt' },
			{ name:'throw_attempt_distance_title', type:'bmptext', parent:'throw_attempt', x:75, y:7, align:'center', fitWidth:138, text:((lang.code == "he") ? ' 1'+lang.getTerm('attempt') : lang.getTerm('attempt')+' 1'), size:'20px', font:lang.getFontName(), tint:0xffffff },
			{ name:'throw_attempt_bone', type:'sprite', id:'AttemptBoneComplete', parent:'throw_attempt', x:15, y:45 },
			{ name:'throw_attempt_distance_txt', type:'bmptext', parent:'throw_attempt', x:70, y:55, fitWidth:70, text:'0', size:'32px', font:lang.getFontName(), tint:0x0},/////////////////////current attempt
			{ name:'next_btn', type:'sprite', id:'StoryNextButton', regX:59, regY:59, visible:false },

			{ name:'collect_star_panel', type:'container', regY:271, x: vp.right - 100, y:vp.bottom},
				{ name:'collectible_tab', type:'sprite', id:'collectible_tab', parent:'collect_star_panel', regX:132, y: 30 },
				{ name:'collectible_sunburst', type:'sprite', id:'collectible_sunburst', parent:'collect_star_panel', regX:157, regY:158, y:165, x:5 },
				//{ name:'collectible_star', type:'sprite', id:'collectible_star', parent:'collect_star_panel', regX:64, regY:65, y:165, x:5 },
		], this.container);

		console.log("TERM = ",lang.getTerm('m'))
		var next_btn = new Button(this.children.next_btn, {scale:1.1}, {
			click: function(){
                soundManager.playButtonSound();
				game.finalizeEndThrow();
				this.hideBubbles();
			}.bind(this)
		});
		
		
		//create the start burst animation
		
		var animationArray = [];
		for(var i=7; i<=60; i++)
		{
			var ii = i < 10 ? '0'+i : i;
			var spriteName = 'starcollect_00'+ ii +'.png';
			animationArray.push( new PIXI.Texture.fromFrame(spriteName) );
		}
		this.starburst = new PIXI.extras.MovieClip(animationArray);
		this.starburst.animationSpeed = 2;
		this.starburst.pivot.x = this.starburst.pivot.y = 60;
		this.starburst.x = -20; 
		this.starburst.y = 135; 
		this.starburst.scale.x = this.starburst.scale.y = 1.2;
		
		this.children.collect_star_panel.addChild( this.starburst );
		
		this.starburst.stop();
		
		var star_btn = new Button(this.starburst, null, {
			click: function(){
                soundManager.playCollectStarSound();
				this.collectStar();
			}.bind(this)
		});
		
        this.elements = [next_btn, star_btn];
    },
    resize: function() {
        var c = this.children, g = app.guiSize, vp = app.viewPort;
		d.applyProps(c.top_container, { x:vp.centerX, y: vp.top });
		d.applyProps(c.stars_collected_cont, { x:vp.right-10, y: vp.top+10 });
		d.applyProps(c.throw_hud, { x:vp.centerX, y: vp.bottom-100 });
		d.applyProps(c.next_btn, { x:vp.right - 59 - 30, y: vp.bottom-59- 30 });
    },
	hasShownHowToPlay: false,
	showThrowComplete: function(){
		var distance = game.attempts[ game.currentThrow - 1 ][ 1 ];
		
		
		this.hideStar();
		
		if(distance > 0) {
			
			this.children.attempt_popup_distance_txt.text =  lang.getTerm('m').replace('p', distance);  
			
			d.applyThenAnimate(this.children.attempt_popup_cont, { visible:true, alpha:0 }, .4, { alpha:1, delay:1.5 });
			d.applyThenAnimate(this.children.next_btn, { visible:true, scale:0 }, .4, { ease:Back.easeOut, scale:1, delay:2 });

			//show the next button
			this.elements[0].enable();
			
		} else {
			d.applyThenAnimate(this.children.foul_throw_bubble, { visible:true, alpha:0 }, .4, { alpha:1, delay:.5 });
			d.applyThenAnimate(this.children.next_btn, { visible:true, scale:0 }, .4, { ease:Back.easeOut, scale:1, delay:1 });
			
			this.elements[0].enable();
		}
		///wind_cont, distance_cont,stars_collected_cont, throw_hud
	},
	hideBubbles: function(){
		this.children.attempt_popup_cont.visible = false;
		this.children.foul_throw_bubble.visible = false;
		
		this.children.next_btn.visible = false;
		this.elements[0].disable();
	},
	showStar: function(){
		
		var type = m.randomInt(1,10);
		
		var x = 0, y = 0, xDiff = 0, yDiff = 0, rotation = 0;
		var vp = app.viewPort;
		
		this.elements[1].enable();
		
		switch(type) {
			case 1:
			case 2:
			case 3:
			case 4:
				x = vp.left;
				xDiff = -100;
				rotation = 90;
				y = m.randomInt(vp.top + 170, vp.bottom -100);
			break;
			case 5:
			case 6:
			case 7:
			case 8:
				x = vp.right;
				xDiff = 100;
				rotation = -90;
				y = m.randomInt(vp.top + 170, vp.bottom -100);
			break;
			case 9:
			case 10:
				x = [vp.left + 120, vp.right - 120][m.randomInt(0,1)];
				y = vp.bottom;
				yDiff = 100;
				rotation = 0;
			break;
		}
		
		//v.get('ingamehud').showStar();
		
		this.starburst.gotoAndStop(1);
		
		var delay = 2-(m.randomInt(0, 12)*0.1);
		//v.get('ingamehud').showStar();
		d.apply(this.children.collectible_sunburst, { scale:0, visible:false });
		d.apply(this.children.collectible_tab, { alpha:1});
		
		d.applyThenAnimate(this.children.collect_star_panel, { rotation:rotation, x:x+xDiff, y:y+yDiff, alpha:0, visible: true }, .2, { x:x, y:y, alpha: 1 });
		
		d.animateObject(this.children.collect_star_panel, .1, { delay:delay, alpha: 0, x:x+xDiff, y:y+yDiff, onStart: function(){
			this.elements[1].disable();
		}.bind(this)});
	},
	hideStar: function(){
		d.killAnimations(this.children.collect_star_panel);
		this.elements[1].disable();
		d.animate(this.children.collect_star_panel, .2, { alpha:0, onComplete:function(){
			this.children.collect_star_panel.visible = false;
		}.bind(this)});
	},
	collectStar: function(){
		d.killAnimations(this.children.collect_star_panel);
		
		game.collectStar();
		this.children.stars_collected_txt.text = (game.stars < 10 ? '0' : '') + game.stars;
		
		this.elements[1].disable();

		
		this.starburst.play();
		
		d.applyThenAnimate(this.children.collectible_sunburst, { visible:true, alpha:0, scale:0 }, .6, {alpha:1, scale:1, onUpdate:function(){
			d.apply(this.children.collectible_sunburst, { rotation: m.radiansToAngle(this.children.collectible_sunburst.rotation) + 10 });
		}.bind(this)});
		
		d.animate(this.children.collectible_tab, .6,  { alpha:0.3 });
				
		d.animate(this.children.collect_star_panel, .2, { delay: .4, alpha:0 });
	},
    transitionIn: function(){
		
		if(!this.hasShownHowToPlay) {
			console.log('show how to play');
			v.get('howtoplay').transitionIn();
			this.hasShownHowToPlay = true;
			return;
		}
		
		this.buildView();
		this.show();
		
		app.addResizeListener('ingame', this.resize.bind(this));
		
		d.applyThenAnimate(this.container, { alpha:0}, .3, { alpha:1});
		
		//wind_cont, distance_cont,stars_collected_cont, throw_hud
		//d.applyThenAnimate(this
		
		this.updateStars();
		
		//start the game
		v.get('ingame').startGame();
    },
	updateDistance : function(distance){
		if(this.container.children.length == 0) return;
		
        this.children.distance_txt.text = lang.getTerm('m').replace('p', distance);  
		
		//console.log(distance);
		if(game.state == GAME_STATE.WALKING) distance = 0;

        this.children.throw_attempt_distance_txt.text = lang.getTerm('m').replace('p', distance);  
	},
	updateAttempts: function(attempts){
		var c = this.children;
		
		//sort out active panel
		c.throw_attempt_distance_txt.text = lang.getTerm('m').replace('p', '0');
		
		if(game.currentThrow < 3) {
			c.throw_attempt_distance_title.text = lang.getTerm('attempt').replace('#', game.currentThrow+1);
			d.animateObject(c.throw_attempt, .3, { x: c['throw_info_' + (game.currentThrow+1)].x });
		} else {
			d.animateObject(c.throw_attempt, .3, { alpha: 0 });
		}
		
		//llop attempts and update attempt hud if throw completed
		for( var i = 0; i < attempts.length; i++ ){
			var at = attempts[i];
			if(at[1] == 0) continue;
			
			c['throw_info_'+(i+1)+'_complete'].visible = true;
			c['throw_info_'+(i+1)+'_inactive'].visible = false;
			c['throw_info_'+(i+1)+'_distance_txt'].text = at[1] == -1 ? '-' : lang.getTerm('m').replace('p', at[1]);
		}
	},
	updateWindSpeed: function(speed){
		if(this.container.children.length == 0 || game.state != GAME_STATE.SWINGING) return;
		
		
        this.children.wind_speed_txt.text = (Math.floor(speed) > 0 ? '+' : '') + speed;
		this.children.wind_speed_txt.updateText();
		
		this.children.windmph_title.position.x = 85 + this.children.wind_speed_txt.textWidth + 5;
		
		if(lang.getFontLTR()) {
			this.children.windmph_title.position.x = 85;
			this.children.wind_speed_txt.position.x = 85 + this.children.windmph_title.textWidth - (lang.code=='ar' ? 30 : 18);
		}
			
		this.children.wind_arrow_good.visible = speed >= 0;
		this.children.wind_arrow_bad.visible = speed < 0;
	},
	updateStars: function(){
		this.children.stars_collected_txt.text = game.stars;
	},
	updateHeight: function(){
		
	},
	updateAttempt: function(m){
		this.children.throw_attempt_distance_txt.text = m+"m";
	},
    transitionOut: function(){
		app.removeResizeListener('ingame');
		
		this.destroyView();  
		this.hide();
    }
});