<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.3.4</string>
        <key>fileName</key>
        <string>//KOKO-NAS/Qweb/kinderfetch/_textures/starburst.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename>../img/starburst.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../img/starburst.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>starburst/starcollect_0017.png</filename>
            <filename>starburst/starcollect_0018.png</filename>
            <filename>starburst/starcollect_0019.png</filename>
            <filename>starburst/starcollect_0020.png</filename>
            <filename>starburst/starcollect_0021.png</filename>
            <filename>starburst/starcollect_0022.png</filename>
            <filename>starburst/starcollect_0023.png</filename>
            <filename>starburst/starcollect_0024.png</filename>
            <filename>starburst/starcollect_0025.png</filename>
            <filename>starburst/starcollect_0026.png</filename>
            <filename>starburst/starcollect_0027.png</filename>
            <filename>starburst/starcollect_0028.png</filename>
            <filename>starburst/starcollect_0029.png</filename>
            <filename>starburst/starcollect_0030.png</filename>
            <filename>starburst/starcollect_0031.png</filename>
            <filename>starburst/starcollect_0032.png</filename>
            <filename>starburst/starcollect_0033.png</filename>
            <filename>starburst/starcollect_0034.png</filename>
            <filename>starburst/starcollect_0035.png</filename>
            <filename>starburst/starcollect_0036.png</filename>
            <filename>starburst/starcollect_0037.png</filename>
            <filename>starburst/starcollect_0038.png</filename>
            <filename>starburst/starcollect_0039.png</filename>
            <filename>starburst/starcollect_0040.png</filename>
            <filename>starburst/starcollect_0041.png</filename>
            <filename>starburst/starcollect_0042.png</filename>
            <filename>starburst/starcollect_0043.png</filename>
            <filename>starburst/starcollect_0044.png</filename>
            <filename>starburst/starcollect_0045.png</filename>
            <filename>starburst/starcollect_0046.png</filename>
            <filename>starburst/starcollect_0047.png</filename>
            <filename>starburst/starcollect_0048.png</filename>
            <filename>starburst/starcollect_0049.png</filename>
            <filename>starburst/starcollect_0050.png</filename>
            <filename>starburst/starcollect_0051.png</filename>
            <filename>starburst/starcollect_0052.png</filename>
            <filename>starburst/starcollect_0053.png</filename>
            <filename>starburst/starcollect_0054.png</filename>
            <filename>starburst/starcollect_0055.png</filename>
            <filename>starburst/starcollect_0056.png</filename>
            <filename>starburst/starcollect_0057.png</filename>
            <filename>starburst/starcollect_0058.png</filename>
            <filename>starburst/starcollect_0059.png</filename>
            <filename>starburst/starcollect_0060.png</filename>
            <filename>starburst/starcollect_0011.png</filename>
            <filename>starburst/starcollect_0012.png</filename>
            <filename>starburst/starcollect_0013.png</filename>
            <filename>starburst/starcollect_0014.png</filename>
            <filename>starburst/starcollect_0015.png</filename>
            <filename>starburst/starcollect_0016.png</filename>
            <filename>starburst/starcollect_0009.png</filename>
            <filename>starburst/starcollect_0007.png</filename>
            <filename>starburst/starcollect_0008.png</filename>
            <filename>starburst/starcollect_0010.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
